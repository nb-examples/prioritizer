package main

import (
	"prioritizer/config"
)

var cnf = config.Config{}

func init() {
	cnf.ReadConfig("./var/config")
}

func main() {
	application := NewApplication(&cnf)
	application.Run()
}
