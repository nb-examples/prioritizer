package config

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"gitlab.action-media.ru/golang/log"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)

type AppType string

const ScheduleApp = AppType("scheduleapp")
const WebApp = AppType("webapp")

type Config struct {
	AppType          AppType          `envconfig:"WORK_MODE"`
	PostgreSql       PostgreSql       `envconfig:"db"`
	Prioritization   Prioritization
	SegSegmentatorDb SegSegmentatorDb `yaml:"seg_segmentator_db" envconfig:"segmentator_db"`
	SegWebpannelDb   SegWebpannelDb   `yaml:"seg_webpannel_db" envconfig:"webpannel_db"`
	Aws              Aws              `yaml:"aws"`
	Amqp             Amqp             `envconfig:"amqp"`
}

type S3 struct {
	Host            string `yaml:"host"`
	AccessKeyId     string `yaml:"access_key_id"`
	SecretAccessKey string `yaml:"secret_access_key"`
	Region          string `yaml:"region"`
	Bucket          string `yaml:"bucket"`
}

type Aws struct {
	S3 S3 `yaml:"s3"`
}

type PostgreSql struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	DBName   string `yaml:"db_name" envconfig:"name"`
}

type SegSegmentatorDb struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	DBName   string `yaml:"db_name"`
}

type SegWebpannelDb struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	DBName   string `yaml:"db_name"`
}

type Prioritization struct {
	GoroutinesCount              int `yaml:"goroutine_count"`
	MaxCountBatch                int `yaml:"max_count_batch"`
	MaxCountUsersOnBatch         int `yaml:"max_count_users_on_batch"`
	UsersBatchesGoroutinesCount  int `yaml:"users_batches_goroutines_count"`
	SingleUserGoroutinesCount    int `yaml:"single_user_goroutines_count"`
	DeletedUsersToSaveBatchCount int `yaml:"deleted_users_to_save_batch_count"`
}

type Amqp struct {
	Dsn string `yaml:"dsn"`
}

func (cnf *Config) ReadConfig(configPath string) {
	env := os.Getenv("ENVIRONMENT")
	if env == "" {
		env = "local"
	}
	log.Info(fmt.Sprintf("ENVIRONMENT=%s", env))

	configRaw, err := ioutil.ReadFile(fmt.Sprintf("%s/config.%s.yml", configPath, env))
	if err != nil {
		log.Fatal(err.Error())
	}

	cnf.setDefaultParams()

	err = yaml.Unmarshal(configRaw, cnf)
	if err != nil {
		log.Fatal(fmt.Sprintf("config parse error: %v", err))
	}

	err = envconfig.Process("APP", cnf)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func (cnf *Config) setDefaultParams() {
	cnf.AppType = WebApp
}

func (cnf *Config) rewriteParam(configVariable string, envVariable string) string {
	param := configVariable

	if envVariable != "" {
		param = envVariable
	}

	return param
}
