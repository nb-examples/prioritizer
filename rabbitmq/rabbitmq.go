package rabbitmq

import (
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.action-media.ru/golang/log"
	"prioritizer/config"
)

type RabbitMQ struct {
	ConnectionRabbitMQ *ConnectionRabbitMQ
}

type ConnectionRabbitMQ struct {
	*amqp.Connection
}

func (rabbitmq *RabbitMQ) InitConnection(config *config.Config) {
	connection, err := amqp.Dial(config.Amqp.Dsn)
	if err != nil {
		log.Fatal(fmt.Sprintf("Error creating RabbitMQ Client: %s", err.Error()))
	}
	rabbitmq.ConnectionRabbitMQ = &ConnectionRabbitMQ{connection}
}
