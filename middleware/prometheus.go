package middleware

import (
	"github.com/valyala/fasthttp"
	"prioritizer/metrics"
	"strconv"
	"time"
)

type PrometheusMiddleware struct {
	metrics *metrics.Prometheus
}

func NewPrometheusMiddleware(metrics *metrics.Prometheus) *PrometheusMiddleware {
	return &PrometheusMiddleware{metrics: metrics}
}

func (m *PrometheusMiddleware) Process(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return func(c *fasthttp.RequestCtx) {
		path := string(c.Request.URI().Path())
		if path == "/_/metrics" {
			next(c)
			return
		}
		start := time.Now()

		next(c)

		duration := time.Since(start)
		status := strconv.Itoa(c.Response.StatusCode())
		method := string(c.Method())
		host := string(c.Request.Host())

		m.metrics.RequestsDuration.WithLabelValues(status, method, host, path).Observe(duration.Seconds())
		m.metrics.RequestsTotal.WithLabelValues(status, method, host, path).Inc()
	}
}
