package application

type ValidationError struct {
	message string
}

func NewValidationError(message ...string) *ValidationError {
	m := ""
	if len(message) > 0 {
		m = message[0]
	}
	return &ValidationError{message: m}
}

func (err *ValidationError) Error() string {
	if err.message != "" {
		return err.message
	}
	return "validation error"
}

func IsValidationError(err error) bool {
	_, ok := err.(*ValidationError)
	return ok
}

type NotFoundError struct {
	message string
}

func NewNotFoundError(message ...string) *NotFoundError {
	m := ""
	if len(message) > 0 {
		m = message[0]
	}
	return &NotFoundError{message: m}
}

func (err *NotFoundError) Error() string {
	if err.message != "" {
		return err.message
	}
	return "not found"
}

func IsNotFoundError(err error) bool {
	_, ok := err.(*NotFoundError)
	return ok
}
