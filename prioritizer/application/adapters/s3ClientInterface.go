package adapters

import (
	"os"
)

type S3ClientInterface interface {
	GetFileFromBucket(file *os.File, s3FilePath string) (err error)
	PutFileToBucket(filePath string)
	GetFileUrlByKey(key string) (urlStr string)
}
