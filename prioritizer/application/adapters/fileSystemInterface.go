package adapters

import (
	"os"
	"prioritizer/prioritizer/domain"
)

type FileSystemInterface interface {
	ParseCsvFile(file *os.File) (chan domain.CsvFileData, error)
	CreateFileIntoTmpDir(fileName string) (file *os.File, err error)
	RemoveFile(file *os.File) (err error)
	RemoveFileByPath(filePath string) (err error)
	WriteToCsvFile(name string, userExportData <-chan domain.UserExportData) error
	GetBaseName(filePath string) string
}
