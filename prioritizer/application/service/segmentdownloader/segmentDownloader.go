package segmentdownloader

import (
	"gitlab.action-media.ru/golang/log"
	"prioritizer/prioritizer/application/adapters"
	"prioritizer/prioritizer/application/service/segmentdownloader/dto"
	"prioritizer/prioritizer/domain"
	"strconv"
)

const BatchSize = 10000

func NewSegmentDownloader(
	fileSystem adapters.FileSystemInterface,
	s3Client adapters.S3ClientInterface,
	userSendingRepository domain.UserSendingRepositoryInterface,
) *SegmentDownloader {
	return &SegmentDownloader{
		fileSystem:            fileSystem,
		s3Client:              s3Client,
		userSendingRepository: userSendingRepository,
	}
}

type SegmentDownloader struct {
	fileSystem            adapters.FileSystemInterface
	s3Client              adapters.S3ClientInterface
	userSendingRepository domain.UserSendingRepositoryInterface
}

func (service *SegmentDownloader) Invoke(segmentToJob dto.SegmentToJob) {
	batch := make([]domain.UserSending, BatchSize)

	baseName := service.fileSystem.GetBaseName(segmentToJob.FilePath)

	file, err := service.fileSystem.CreateFileIntoTmpDir(baseName)
	if err != nil {
		log.Error(err.Error())
	}

	err = service.s3Client.GetFileFromBucket(file, segmentToJob.FilePath)
	if err != nil {
		log.Error(err.Error())
	}

	result, err := service.fileSystem.ParseCsvFile(file)
	if err != nil {
		log.Error(err.Error())
	}

	for line := range result {
		r, _ := strconv.Atoi(line.UserId)

		batch = append(batch, domain.UserSending{
			JobId:       domain.JobId(segmentToJob.JobId),
			ReleaseId:   domain.ReleaseId(segmentToJob.ReleaseId),
			UserId:      domain.UserId(r),
			UserEmail:   domain.Email(line.Email),
			SegmentId:   domain.SegmentId(segmentToJob.SegmentId),
			Type:        domain.SendingType(segmentToJob.Type),
			ProductId:   domain.ProductId(segmentToJob.ProductId),
			SendingTime: segmentToJob.SendingTime,
		})

		if len(batch) >= BatchSize {
			err = service.userSendingRepository.SaveMultiple(batch)
			if err != nil {
				log.Error(err.Error())
			}

			batch = batch[:0]
		}
	}

	if len(batch) > 0 {
		err = service.userSendingRepository.SaveMultiple(batch)
		if err != nil {
			log.Error(err.Error())
		}
	}

	err = service.fileSystem.RemoveFile(file)
	if err != nil {
		log.Error(err.Error())
	}
}
