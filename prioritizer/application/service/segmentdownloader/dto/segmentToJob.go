package dto

import "time"

type SegmentToJob struct {
	UserSendingId string    `json:"userSendingId"`
	JobId         string    `json:"jobId"`
	ReleaseId     int       `json:"releaseId"`
	SegmentId     int       `json:"segmentId"`
	Type          string    `json:"type"`
	ProductId     int       `json:"productId"`
	SendingTime   time.Time `json:"sendingTime"`
	FilePath      string    `json:"filePath"`
}
