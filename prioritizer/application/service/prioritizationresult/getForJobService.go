package prioritizationresult

import "prioritizer/prioritizer/domain"

type GetPrioritizationResultsService struct {
	prioritizationResultsRepository domain.PrioritizationResultsRepositoryInterface
}

func NewGetPrioritizationResultsService(prioritizationResultsRepository domain.PrioritizationResultsRepositoryInterface) *GetPrioritizationResultsService {
	return &GetPrioritizationResultsService{prioritizationResultsRepository: prioritizationResultsRepository}
}

func (service *GetPrioritizationResultsService) GetByJobId(jobId string) ([]domain.PrioritizationResult, error) {
	return service.prioritizationResultsRepository.GetByJobId(domain.JobId(jobId))
}
