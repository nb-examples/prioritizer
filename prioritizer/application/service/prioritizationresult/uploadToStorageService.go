package prioritizationresult

import (
	"context"
	"errors"
	"fmt"
	"github.com/jinzhu/gorm"
	"gitlab.action-media.ru/golang/log"
	"prioritizer/prioritizer/application/adapters"
	"prioritizer/prioritizer/domain"
	"sync"
)

const TmpFileDir = "/tmp/"
const SegmentFileMask = "%s_job_%d_events.csv"

func NewUploadToStorageService(
	userSendingRepository domain.UserSendingRepositoryInterface,
	prioritizationResultsRepositoryInterface domain.PrioritizationResultsRepositoryInterface,
	storageInterface adapters.S3ClientInterface,
	systemInterface adapters.FileSystemInterface,
) *UploadToStorageService {
	return &UploadToStorageService{
		userSendingRepository:                    userSendingRepository,
		prioritizationResultsRepositoryInterface: prioritizationResultsRepositoryInterface,
		storageInterface:                         storageInterface,
		fileSystemInterface:                      systemInterface,
	}
}

type UploadToStorageService struct {
	userSendingRepository                    domain.UserSendingRepositoryInterface
	prioritizationResultsRepositoryInterface domain.PrioritizationResultsRepositoryInterface
	storageInterface                         adapters.S3ClientInterface
	fileSystemInterface                      adapters.FileSystemInterface
}

func (service *UploadToStorageService) Save(ctx context.Context, jobId domain.JobId) {
	var wg sync.WaitGroup

	select {
	case <-ctx.Done():
		return
	default:
	}

	releaseIds, err := service.userSendingRepository.GetReleaseIdByJobId(jobId)
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return
	}

	if err != nil {
		log.Error(err.Error())
		return
	}

	for _, releaseId := range releaseIds {
		wg.Add(1)

		go func() {
			defer wg.Done()

			userExportData, err := service.userSendingRepository.GetAllUsersByReleaseId(releaseId)
			if err != nil {
				log.Error(err.Error())
				return
			}

			filePath := fmt.Sprintf(TmpFileDir+SegmentFileMask, jobId, releaseId)

			err = service.fileSystemInterface.WriteToCsvFile(filePath, userExportData)
			if err != nil {
				log.Error(err.Error())
				return
			}

			service.storageInterface.PutFileToBucket(filePath)

			err = service.fileSystemInterface.RemoveFileByPath(filePath)
			if err != nil {
				log.Error(err.Error())
			}

			link := service.storageInterface.GetFileUrlByKey(filePath)

			prioritizationResult := domain.PrioritizationResult{
				ReleaseId:  releaseId,
				JobId:      jobId,
				SegmentUrl: domain.SegmentUrl(link),
			}

			err = service.prioritizationResultsRepositoryInterface.Save(&prioritizationResult)
			if err != nil {
				log.Error(err.Error())
				return
			}
		}()
	}

	wg.Wait()
}
