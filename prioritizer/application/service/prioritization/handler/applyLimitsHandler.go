package handler

import (
	"fmt"
	"gitlab.action-media.ru/golang/log"
	"prioritizer/prioritizer/domain"
	"prioritizer/prioritizer/domain/service/prioritization"
)

type ApplyLimitsHandler struct {
	CalculateAvailableLimits prioritization.CalculateAvailableLimitsInterface
}

func NewApplyLimitsHandler(
	limits prioritization.CalculateAvailableLimitsInterface,
) *ApplyLimitsHandler {
	return &ApplyLimitsHandler{
		CalculateAvailableLimits: limits,
	}
}

func (handler *ApplyLimitsHandler) Execute(userSendings *domain.UserSendings) (err error) {
	availableLimits := handler.CalculateAvailableLimits.Invoke(userSendings.UserAllReadySent, userSendings.UserSendingLimit)

	sendings := make([]domain.UserSending, len(userSendings.Sendings))

	copy(sendings, userSendings.Sendings)

	index := 0
	for _, userSending := range sendings {
		if userSending.Type == domain.CLIENT {
			if availableLimits.Client <= 0 {
				log.Debug(fmt.Sprintf("Exclude sending [schedulerId: %d] by client limit [userId: %d]", userSendings.Sendings[index].ReleaseId, userSending.UserId))

				userSendings.ExcludeSending(index)

				index--
			} else {
				availableLimits.Client--
			}
		} else {
			if availableLimits.Market <= 0 {
				log.Debug(fmt.Sprintf("Exclude sending [schedulerId: %d] by marketing limit [userId: %d]", userSendings.Sendings[index].ReleaseId, userSending.UserId))

				userSendings.ExcludeSending(index)

				index--
			} else {
				availableLimits.Market--
			}
		}

		index++
	}

	return
}
