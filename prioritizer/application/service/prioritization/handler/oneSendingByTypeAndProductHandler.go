package handler

import (
	"fmt"
	"gitlab.action-media.ru/golang/log"
	"prioritizer/prioritizer/domain"
)

type OneSendingByTypeAndProductHandler struct{}

func NewOneSendingByTypeAndProductHandler() *OneSendingByTypeAndProductHandler {
	return &OneSendingByTypeAndProductHandler{}
}

func (handler *OneSendingByTypeAndProductHandler) Execute(userSending *domain.UserSendings) (err error) {
	sentProducts := make(map[int]map[string]bool)

	index := 0
	for _, sending := range userSending.Sendings {
		if sending.Type.IsClient() {
			index++
			continue
		}

		sendingType := string(sending.Type)
		sendingProduct := int(sending.ProductId)


		if sentProducts[sendingProduct][sendingType] {
			userSending.ExcludedSendings = append(userSending.ExcludedSendings, sending)
			userSending.Sendings = append(userSending.Sendings[:index], userSending.Sendings[index+1:]...)

			log.Debug(fmt.Sprintf("Exclude sending [schedulerId: %d] (oneSendingByTypeAndProductHandler) [userId: %d]", sending.ReleaseId, sending.UserId))

			index--
		}

		if sentProducts[sendingProduct] == nil {
			sentProducts[sendingProduct] = make(map[string]bool)
		}

		sentProducts[sendingProduct][sendingType] = true

		index++
	}

	return
}
