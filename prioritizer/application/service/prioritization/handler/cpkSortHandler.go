package handler

import (
	"prioritizer/prioritizer/domain"
	"sort"
)

// todo: нужно написать тест, из старого обработчика переносить нет смысла, там тест проверяет только то, что
// обработчик не вернул ошибок
type CpkSortHandler struct {
}

func NewCpkSortHandler() *CpkSortHandler {
	return &CpkSortHandler{}
}

const DefaultCpkValue = 0

func (handler *CpkSortHandler) Execute(userSending *domain.UserSendings) (err error) {
	handler.sortSendings(userSending)

	return
}

func (handler *CpkSortHandler) sortSendings(userSending *domain.UserSendings) {
	sort.Slice(userSending.Sendings, func(i, j int) bool {
		cpkValueOne := handler.getCpkValue(userSending, userSending.Sendings[i])
		cpkValueTwo := handler.getCpkValue(userSending, userSending.Sendings[j])

		if cpkValueOne == cpkValueTwo {
			return userSending.Sendings[i].SendingTime.Before(userSending.Sendings[j].SendingTime)
		}

		return cpkValueOne > cpkValueTwo
	})
}

func (handler *CpkSortHandler) getCpkValue(userSending *domain.UserSendings, sending domain.UserSending) int {
	// todo: нужно сделать userSending.UserProductsCpk в виде мапы с ключом id продукта
	for _, item := range userSending.UserProductsCpk {
		if sending.ProductId == item.ProductId {
			return item.Value
		}
	}

	// Default value if cpk score does`t exists
	return DefaultCpkValue
}
