package handler

import (
	"prioritizer/prioritizer/domain"
)

type AdvReleaseHandler struct {
}

func NewAdvReleaseHandler() *AdvReleaseHandler {
	return &AdvReleaseHandler{}
}

func (handler *AdvReleaseHandler) Execute(userSending *domain.UserSendings) (err error) {
	isNotExistsTypeInSendingAddOne(userSending, domain.ADV)

	return
}
