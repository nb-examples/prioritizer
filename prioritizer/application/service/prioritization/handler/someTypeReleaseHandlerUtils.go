package handler

import (
	"fmt"
	"gitlab.action-media.ru/golang/log"
	"prioritizer/prioritizer/domain"
	"sort"
)

func sortBySendDate(sending []domain.UserSending) []domain.UserSending {
	sort.Slice(sending, func(i, j int) bool {
		return sending[i].SendingTime.Before(sending[j].SendingTime)
	})

	return sending
}

func isExistsTypeInSending(sending []domain.UserSending, mailingType string) bool {
	for _, mailing := range sending {
		if string(mailing.Type) == mailingType {
			return true
		}
	}

	return false
}

func isNotExistsTypeInSendingAddOne(userSending *domain.UserSendings, sendingType domain.SendingType)  {
	if isExistsTypeInSending(userSending.Sendings, string(sendingType)) {
		log.Debug(fmt.Sprintf("Type %s found in sendings [userId: %d]", sendingType, userSending.UserId))

		return
	}

	if isExistsTypeInSending(userSending.ExcludedSendings, string(sendingType)) {
		userSending.ExcludedSendings = sortBySendDate(userSending.ExcludedSendings)

		for index, mailing := range userSending.ExcludedSendings {
			if string(mailing.Type) == string(sendingType) {
				userSending.ExcludedSendings = append(userSending.ExcludedSendings[:index], userSending.ExcludedSendings[index+1:]...)
				userSending.Sendings = append(userSending.Sendings, mailing)

				log.Debug(fmt.Sprintf("Return sending [schedulerId: %d] type %s from excluded [userId: %d]", mailing.ReleaseId, sendingType, userSending.UserId))

				return
			}
		}
	}
}
