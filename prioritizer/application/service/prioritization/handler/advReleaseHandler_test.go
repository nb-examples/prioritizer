package handler

import (
	"prioritizer/prioritizer/domain"
	"reflect"
	"testing"
	"time"
)

var advReleaseHandler = NewAdvReleaseHandler()

func TestAdvReleaseHandler(t *testing.T) {
	userSendings := domain.UserSendings{
		UserId: 1,
		Sendings: []domain.UserSending{
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				1,
				1,
				"test1@test.test",
				1,
				"demo",
				5,
				time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				2,
				1,
				"test1@test.test",
				2,
				"demo",
				6,
				time.Date(2020, 9, 3, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				3,
				1,
				"test1@test.test",
				3,
				"demo",
				10,
				time.Date(2020, 9, 1, 00, 00, 00, 651387237, time.UTC),
			},
		},
		ExcludedSendings: []domain.UserSending{
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				4,
				1,
				"test1@test.test",
				3,
				"adv",
				10,
				time.Date(2020, 9, 1, 02, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				5,
				1,
				"test1@test.test",
				3,
				"adv",
				10,
				time.Date(2020, 9, 1, 00, 00, 00, 651387237, time.UTC),
			},
		},
	}

	expectedUserSendings := domain.UserSendings{
		UserId: 1,
		Sendings: []domain.UserSending{
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				1,
				1,
				"test1@test.test",
				1,
				"demo",
				5,
				time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				2,
				1,
				"test1@test.test",
				2,
				"demo",
				6,
				time.Date(2020, 9, 3, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				3,
				1,
				"test1@test.test",
				3,
				"demo",
				10,
				time.Date(2020, 9, 1, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				5,
				1,
				"test1@test.test",
				3,
				"adv",
				10,
				time.Date(2020, 9, 1, 00, 00, 00, 651387237, time.UTC),
			},
		},
	}

	_ = advReleaseHandler.Execute(&userSendings)

	if false == reflect.DeepEqual(expectedUserSendings.Sendings, userSendings.Sendings) {
		t.Errorf("Test execute when one type failed, expected %+v, got %+v", expectedUserSendings, userSendings)
	}
}
