package handler

import (
	"prioritizer/prioritizer/domain"
	"reflect"
	"testing"
	"time"
)

var h = NewOneSendingByTypeAndProductHandler()

func TestExecute(t *testing.T) {
	userSending := domain.UserSendings{
		UserId: 1,
		Sendings: []domain.UserSending{
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				1,
				1,
				"test1@test.test",
				1,
				"demo",
				5,
				time.Date(2020, 9, 3, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				2,
				1,
				"test1@test.test",
				2,
				"demo",
				5,
				time.Date(2020, 9, 1, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				3,
				1,
				"test1@test.test",
				5,
				"market",
				6,
				time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fs2",
				3,
				1,
				"testclient@test.test",
				2,
				"client",
				7,
				time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fs2",
				3,
				1,
				"testclient@test.test",
				2,
				"client",
				7,
				time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
		},
	}

	_ = h.Execute(&userSending)

	expectedUserSendings := domain.UserSendings{
		UserId: 1,
		Sendings: []domain.UserSending{
			{

				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				1,
				1,
				"test1@test.test",
				1,
				"demo",
				5,
				time.Date(2020, 9, 3, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				3,
				1,
				"test1@test.test",
				5,
				"market",
				6,
				time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fs2",
				3,
				1,
				"testclient@test.test",
				2,
				"client",
				7,
				time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fs2",
				3,
				1,
				"testclient@test.test",
				2,
				"client",
				7,
				time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
		},
		ExcludedSendings: []domain.UserSending{
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				2,
				1,
				"test1@test.test",
				2,
				"demo",
				5,
				time.Date(2020, 9, 1, 00, 00, 00, 651387237, time.UTC),
			},
		},
	}

	if !reflect.DeepEqual(expectedUserSendings, userSending) {
		t.Errorf("Test execute when one product failed, expected %+v, got %+v", expectedUserSendings, userSending)
	}
}
