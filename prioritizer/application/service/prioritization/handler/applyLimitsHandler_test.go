package handler

import (
	"prioritizer/prioritizer/domain"
	"prioritizer/prioritizer/domain/service/prioritization"
	"reflect"
	"testing"
	"time"
)

var applyLimitsHandler = NewApplyLimitsHandler(
	NewApplyLimitsHandlerCalculateAvailableLimitsMock(),
)

type ApplyLimitsHandlerCalculateAvailableLimitsMock struct{}

func NewApplyLimitsHandlerCalculateAvailableLimitsMock() prioritization.CalculateAvailableLimitsInterface {
	return &ApplyLimitsHandlerCalculateAvailableLimitsMock{}
}

func (calculateAvailableLimits *ApplyLimitsHandlerCalculateAvailableLimitsMock) Invoke(log domain.LimiterLog, limit domain.UserSendingLimit) (availableLimitForUser prioritization.AvailableLimitForUser) {
	return prioritization.AvailableLimitForUser{
		Market: 1,
		Client: 1,
	}
}

func TestApplyLimitsHandler(t *testing.T) {
	userSending := domain.UserSendings{
		UserId: 1,
		Sendings: []domain.UserSending{
			{
				JobId:       "91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				ReleaseId:   1,
				UserId:      1,
				UserEmail:   "test1@test.test",
				SegmentId:   1,
				Type:        "demo",
				ProductId:   5,
				SendingTime: time.Date(2020, 9, 3, 00, 00, 00, 651387237, time.UTC),
			},
			{
				JobId:       "91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				ReleaseId:   2,
				UserId:      1,
				UserEmail:   "test1@test.test",
				SegmentId:   2,
				Type:        "demo",
				ProductId:   5,
				SendingTime: time.Date(2020, 9, 1, 00, 00, 00, 651387237, time.UTC),
			},
			{
				JobId:       "91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				ReleaseId:   3,
				UserId:      1,
				UserEmail:   "test1@test.test",
				SegmentId:   5,
				Type:        "market",
				ProductId:   6,
				SendingTime: time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
			{
				JobId:       "91ea5e08-aa85-487c-8d7b-ffcb4b9b1fs2",
				ReleaseId:   4,
				UserId:      1,
				UserEmail:   "testclient@test.test",
				SegmentId:   2,
				Type:        "client",
				ProductId:   7,
				SendingTime: time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
			{
				JobId:       "91ea5e08-aa85-487c-8d7b-ffcb4b9b1fs2",
				ReleaseId:   5,
				UserId:      1,
				UserEmail:   "testclient@test.test",
				SegmentId:   2,
				Type:        "client",
				ProductId:   7,
				SendingTime: time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
		},
	}

	_ = applyLimitsHandler.Execute(&userSending)

	expectedUserSendings := domain.UserSendings{
		UserId: 1,
		Sendings: []domain.UserSending{
			{
				JobId:       "91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				ReleaseId:   1,
				UserId:      1,
				UserEmail:   "test1@test.test",
				SegmentId:   1,
				Type:        "demo",
				ProductId:   5,
				SendingTime: time.Date(2020, 9, 3, 00, 00, 00, 651387237, time.UTC),
			},
			{
				JobId:       "91ea5e08-aa85-487c-8d7b-ffcb4b9b1fs2",
				ReleaseId:   4,
				UserId:      1,
				UserEmail:   "testclient@test.test",
				SegmentId:   2,
				Type:        "client",
				ProductId:   7,
				SendingTime: time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
		},
		ExcludedSendings: []domain.UserSending{
			{
				JobId:       "91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				ReleaseId:   2,
				UserId:      1,
				UserEmail:   "test1@test.test",
				SegmentId:   2,
				Type:        "demo",
				ProductId:   5,
				SendingTime: time.Date(2020, 9, 1, 00, 00, 00, 651387237, time.UTC),
			},
			{
				JobId:       "91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				ReleaseId:   3,
				UserId:      1,
				UserEmail:   "test1@test.test",
				SegmentId:   5,
				Type:        "market",
				ProductId:   6,
				SendingTime: time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
			{
				JobId:       "91ea5e08-aa85-487c-8d7b-ffcb4b9b1fs2",
				ReleaseId:   5,
				UserId:      1,
				UserEmail:   "testclient@test.test",
				SegmentId:   2,
				Type:        "client",
				ProductId:   7,
				SendingTime: time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
		},
	}

	if !reflect.DeepEqual(expectedUserSendings, userSending) {
		t.Errorf("Test execute apply limits failed, expected %+v, got %+v", expectedUserSendings, userSending)
	}
}
