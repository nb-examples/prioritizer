package handler

import (
	"prioritizer/prioritizer/domain"
)

type ClientReleaseHandler struct {
}

func NewClientReleaseHandler() *ClientReleaseHandler {
	return &ClientReleaseHandler{}
}

func (handler *ClientReleaseHandler) Execute(userSending *domain.UserSendings) (err error) {
	isNotExistsTypeInSendingAddOne(userSending, domain.CLIENT)

	return
}
