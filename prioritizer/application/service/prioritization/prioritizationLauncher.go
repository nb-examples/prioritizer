package prioritization

import (
	"fmt"
	"gitlab.action-media.ru/golang/log"
	"prioritizer/prioritizer/application/job"
	"prioritizer/prioritizer/domain/service/prioritization"
	"time"
)

type PrioritizationLauncher struct {
	jobService            *job.JobService
	prioritizationService prioritization.PrioritizationServiceInterface
}

func NewPrioritizationLauncher(
	jobService *job.JobService,
	prioritizationService prioritization.PrioritizationServiceInterface,
) *PrioritizationLauncher {
	return &PrioritizationLauncher{
		jobService:            jobService,
		prioritizationService: prioritizationService,
	}
}

type PrioritizeRequestDto struct {
	Day time.Time
}

func (service *PrioritizationLauncher) LaunchPrioritization(request PrioritizeRequestDto) (err error) {
	job, err := service.jobService.GetByDay(request.Day)
	if nil != err {
		return
	}

	if nil != job {
		return
	}

	job, err = service.jobService.Create(request.Day)
	if nil != err {
		return
	}

	log.Debug(fmt.Sprintf("Launch prioritization (job [%s]) at %s", job.Id, time.Now()))

	go service.prioritizationService.Prioritize(*job)

	return
}
