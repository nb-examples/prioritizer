package prioritization

import (
	"prioritizer/prioritizer/application/service/prioritization/handler"
	"prioritizer/prioritizer/domain/service/prioritization"
)

// todo: CpkTrimHandler кидает панику
// todo: remove old handlers files
type HandlerChainFactory struct {
	handler1 *handler.CpkSortHandler // сортировка по cpk и дате выхода
	handler2 *handler.OneSendingByTypeAndProductHandler // не больше одной рассылки по 1 типу и 1 продукту
	handler3 *handler.ApplyLimitsHandler // применяем лимиты
	handler4 *handler.AdvReleaseHandler // вернуть 1 adv рассылку, ели ее нет
	handler5 *handler.ClientReleaseHandler // вернуть 1 клиентскую, если ни одной нет
}

func NewHandlerChainFactory(
	handler1 *handler.CpkSortHandler,
	handler2 *handler.OneSendingByTypeAndProductHandler,
	handler3 *handler.ApplyLimitsHandler,
	handler4 *handler.AdvReleaseHandler,
	handler5 *handler.ClientReleaseHandler,
) prioritization.HandlerFactoryInterface {
	return &HandlerChainFactory{
		handler1: handler1,
		handler2: handler2,
		handler3: handler3,
		handler4: handler4,
		handler5: handler5,
	}
}

func (factory HandlerChainFactory) GetHandler() prioritization.HandlerInterface {
	composite := prioritization.CompositeHandler{}
	composite.Add(factory.handler1)
	composite.Add(factory.handler2)
	composite.Add(factory.handler3)
	composite.Add(factory.handler4)
	composite.Add(factory.handler5)

	return composite
}
