package job

import (
	"errors"
	"fmt"
	"github.com/avast/retry-go"
	"gitlab.action-media.ru/golang/log"
	"prioritizer/prioritizer/domain"
	"prioritizer/prioritizer/domain/service/prioritization"
	"time"
)

type JobService struct {
	jobRepository         domain.JobRepositoryInterface
	prioritizationService prioritization.PrioritizationService
}

func NewJobService(jobRepository domain.JobRepositoryInterface) *JobService {
	return &JobService{
		jobRepository: jobRepository,
	}
}

func (service JobService) GetById(jobId domain.JobId) (*domain.Job, error) {
	job, err := service.jobRepository.GetById(jobId)
	if err != nil {
		return nil, err
	}
	return job, err
}

func (service JobService) GetByDay(day time.Time) (*domain.Job, error) {
	job, err := service.jobRepository.GetByDay(day)
	if err != nil {
		return nil, err
	}
	return job, err
}

func (service JobService) ReLaunch(job *domain.Job) (err error) {
	if job.IsFailed() {
		job.MarkReLaunched()

		err = service.saveJob(job)
		if err != nil {
			return err
		}

		go service.prioritizationService.Prioritize(*job)
	} else {
		return errors.New(fmt.Sprintf("Re-launch is impossable, job: %s in status %s", job.Id, job.Status))
	}

	return
}

func (service JobService) Create(day time.Time) (job *domain.Job, err error) {
	job = &domain.Job{
		Id:        service.jobRepository.GetNextIdentity(),
		Status:    domain.New,
		Day:       day,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}
	
	err = service.saveJob(job)
	if err != nil {
		return
	}

	return
}

func (service JobService) saveJob(job *domain.Job) (err error) {
	err = retry.Do(
		func() (err error) {
			err = service.jobRepository.Save(job)
			return
		},
		retry.OnRetry(func(n uint, err error) {
			log.Debug(fmt.Sprintf("Retry save job #%d: %s\n", n, err))
		}),
		retry.Attempts(3),
	)
	if err != nil {
		return
	}
	return
}
