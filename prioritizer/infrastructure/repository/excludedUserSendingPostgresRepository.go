package repository

import (
	"prioritizer/db"
	"prioritizer/prioritizer/domain"
)

func NewExcludedUserSendingPostgresRepository(connection *db.ConnectionPgSql) domain.ExcludedUserSendingRepositoryInterface {
	return &ExcludedUserSendingPostgresRepository{connection}
}

type ExcludedUserSendingPostgresRepository struct {
	connection *db.ConnectionPgSql
}

func (repository *ExcludedUserSendingPostgresRepository) Save(excluded []domain.UserSending) error {
	return repository.connection.Create(excluded).Error
}

func (repository *ExcludedUserSendingPostgresRepository) TableName() string {
	return "excluded_user_sendings"
}
