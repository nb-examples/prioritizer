package repository

import (
	"prioritizer/db"
	"prioritizer/prioritizer/domain"
)

type PrioritizationResultsPostgresRepository struct {
	connection *db.ConnectionPgSql
}

func NewPrioritizationResultsPostgresRepository(
	connection *db.ConnectionPgSql,
) domain.PrioritizationResultsRepositoryInterface {
	return &PrioritizationResultsPostgresRepository{connection: connection}
}

func (repository PrioritizationResultsPostgresRepository) Save(prioritizationResult *domain.PrioritizationResult) error {
	return repository.connection.Save(prioritizationResult).Error
}

func (repository PrioritizationResultsPostgresRepository) GetByJobId(jobId domain.JobId) (prioritizationResults []domain.PrioritizationResult, err error) {
	err = repository.connection.Where("job_id = ?", string(jobId)).Find(&prioritizationResults).Error
	return
}
