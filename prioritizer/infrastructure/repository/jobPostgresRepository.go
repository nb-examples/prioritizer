package repository

import (
	"errors"
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"prioritizer/db"
	"prioritizer/prioritizer/domain"
	"time"
)

type JobPostgresRepository struct {
	connection *db.ConnectionPgSql
}

func NewJobPostgresRepository(connection *db.ConnectionPgSql) domain.JobRepositoryInterface {
	return &JobPostgresRepository{connection: connection}
}

func (repository JobPostgresRepository) GetNextIdentity() domain.JobId {
	return domain.JobId(uuid.NewV4().String())
}

func (repository JobPostgresRepository) GetById(jobId domain.JobId) (*domain.Job, error) {
	job := domain.Job{}
	err := repository.connection.Where("id = ?", jobId).First(&job).Error
	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	return &job, err
}


func (repository JobPostgresRepository) GetByDay(day time.Time) (*domain.Job, error) {
	job := domain.Job{}
	err := repository.connection.Where("day = ?", day.Format("2006-01-02")).First(&job).Error
	if err != nil && errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	return &job, err
}

func (repository JobPostgresRepository) GetByStatus(status domain.JobStatus) (jobs []domain.Job, err error) {
	err = repository.connection.Where("status = ?", string(status)).Find(&jobs).Error
	return
}

func (repository JobPostgresRepository) Save(job *domain.Job) error {
	return repository.connection.Save(job).Error
}
