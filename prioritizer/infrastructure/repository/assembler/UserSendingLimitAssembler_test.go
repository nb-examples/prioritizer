package assembler

import (
	"prioritizer/prioritizer/domain"
	"reflect"
	"testing"
)

func TestUserSendingLimitAssemblerSuccess(t *testing.T) {
	userSending := domain.UserSendings{
		UserId:    1,
		UserEmail: "test1@test.test",
	}

	limits := []domain.UserSendingLimit{
		{
			UserId:      1,
			ClientLimit: 10,
			MarketLimit: 50,
		},
		{
			UserId:      2,
			ClientLimit: 10,
			MarketLimit: 50,
		},
		{
			UserId:      3,
			ClientLimit: 10,
			MarketLimit: 50,
		},
	}

	expectedUserSending := domain.UserSendings{
		UserId:    1,
		UserEmail: "test1@test.test",
		UserSendingLimit: domain.UserSendingLimit{
			UserId:      1,
			ClientLimit: 10,
			MarketLimit: 50,
		},
	}

	UserSendingLimitAssembler(&userSending, limits)

	if false == reflect.DeepEqual(expectedUserSending, userSending) {
		t.Errorf("Test execute when one product failed, expected %+v, got %+v", expectedUserSending, userSending)
	}
}
