package assembler

import (
	"prioritizer/prioritizer/domain"
	"prioritizer/prioritizer/infrastructure/repository/dto"
)

func UserSendingAssembler(userSending *domain.UserSendings, countedUsers map[int][]dto.CountedUser, jobId domain.JobId) {
	countedUserSendings := countedUsers[int(userSending.UserId)]

	for _, countedUserSending := range countedUserSendings {
		sending := domain.UserSending{
			JobId:       jobId,
			ReleaseId:   countedUserSending.SchedulerId,
			UserId:      countedUserSending.UserId,
			UserEmail:   countedUserSending.Email,
			SegmentId:   countedUserSending.SegmentId,
			Type:        countedUserSending.Type,
			ProductId:   countedUserSending.ProductId,
			SendingTime: countedUserSending.SendingTime,
		}

		userSending.Sendings = append(userSending.Sendings, sending)
	}
}
