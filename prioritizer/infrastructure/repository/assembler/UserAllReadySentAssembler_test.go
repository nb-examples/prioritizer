package assembler

import (
	"prioritizer/prioritizer/domain"
	"reflect"
	"testing"
)

func TestUserAllReadySentAssemblerSuccess(t *testing.T) {
	userSending := domain.UserSendings{
		UserId:    1,
		UserEmail: "test1@test.test",
	}

	limits := []domain.LimiterLog{
		{
			Email:               "test1@test.test",
			MarketCount:         1,
			ClientCount:         1,
		},
		{
			Email:               "test1@test.test",
			MarketCount:         2,
			ClientCount:         2,
		},
		{
			Email:               "test1@test.test",
			MarketCount:         3,
			ClientCount:         3,
		},
	}

	expectedUserSending := domain.UserSendings{
		UserId:    1,
		UserEmail: "test1@test.test",
		UserAllReadySent: domain.LimiterLog{
			Email:               "test1@test.test",
			MarketCount:         1,
			ClientCount:         1,
		},
	}

	UserAllReadySentAssembler(&userSending, limits)

	if false == reflect.DeepEqual(expectedUserSending, userSending) {
		t.Errorf("Test execute when one product failed, expected %+v, got %+v", expectedUserSending, userSending)
	}
}
