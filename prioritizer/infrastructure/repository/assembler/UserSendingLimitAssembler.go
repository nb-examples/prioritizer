package assembler

import "prioritizer/prioritizer/domain"

const DefaultMarketLimit = 7
const DefaultClientLimit = 7

func UserSendingLimitAssembler(userSending *domain.UserSendings, userSendingLimit []domain.UserSendingLimit) {
	for _, userSendingLimitData := range userSendingLimit {
		if userSendingLimitData.UserId == userSending.UserId {
			userSending.UserSendingLimit = userSendingLimitData
			return
		}
	}

	userSending.UserSendingLimit = domain.UserSendingLimit{
		UserId:      userSending.UserId,
		MarketLimit: DefaultMarketLimit,
		ClientLimit: DefaultClientLimit,
	}
}
