package assembler

import (
	"prioritizer/prioritizer/domain"
	"reflect"
	"testing"
)

func TestUserProductCpkAssemblerSuccess(t *testing.T) {
	userSending := domain.UserSendings{
		UserId:    1,
		UserEmail: "test1@test.test",
	}

	cpk := []domain.UserProductCpk{
		{
			UserId:    1,
			ProductId: 1,
			Value:     50,
		},
		{
			UserId:    2,
			ProductId: 1,
			Value:     50,
		},
		{
			UserId:    1,
			ProductId: 2,
			Value:     75,
		},
	}

	expectedUserSending := domain.UserSendings{
		UserId:    1,
		UserEmail: "test1@test.test",
		UserProductsCpk: []domain.UserProductCpk{
			{
				UserId:    1,
				ProductId: 1,
				Value:     50,
			},
			{
				UserId:    1,
				ProductId: 2,
				Value:     75,
			},
		},
	}

	UserProductCpkAssembler(&userSending, cpk)

	if false == reflect.DeepEqual(expectedUserSending, userSending) {
		t.Errorf("Test execute when one product failed, expected %+v, got %+v", expectedUserSending, userSending)
	}
}
