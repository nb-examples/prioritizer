package assembler

import "prioritizer/prioritizer/domain"

func UserAllReadySentAssembler(userSending *domain.UserSendings, userAllReadySent []domain.LimiterLog) {
	for _, userAllReadySentData := range userAllReadySent {
		if userAllReadySentData.Email == string(userSending.UserEmail) {
			userSending.UserAllReadySent = userAllReadySentData
			return
		}
	}
}
