package assembler

import "prioritizer/prioritizer/domain"

func UserProductCpkAssembler(userSending *domain.UserSendings, userProductsCpk []domain.UserProductCpk) {
	for _, cpkData := range userProductsCpk {
		if cpkData.UserId == userSending.UserId {
			userSending.UserProductsCpk = append(userSending.UserProductsCpk, cpkData)
		}
	}
}
