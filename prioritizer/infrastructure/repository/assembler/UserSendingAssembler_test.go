package assembler

import (
	"prioritizer/prioritizer/domain"
	"prioritizer/prioritizer/infrastructure/repository/dto"
	"reflect"
	"testing"
	"time"
)

func TestUserSendingAssemblerSuccess(t *testing.T) {
	userSending := domain.UserSendings{
		UserId:    1,
		UserEmail: "test1@test.test",
	}

	countedUsers := map[int][]dto.CountedUser{
		1: {
			dto.CountedUser{
				UserId:      1,
				Email:       "test1@test.test",
				SchedulerId: 1,
				SegmentId:   1,
				Type:        "demo",
				ProductId:   5,
				SendingTime: time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
			dto.CountedUser{
				UserId:      1,
				Email:       "test1@test.test",
				SchedulerId: 1,
				SegmentId:   2,
				Type:        "demo",
				ProductId:   6,
				SendingTime: time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
		},
		3: {
			dto.CountedUser{
				UserId:      1,
				Email:       "test1@test.test",
				SchedulerId: 1,
				SegmentId:   3,
				Type:        "demo",
				ProductId:   10,
				SendingTime: time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
		},
	}

	expectedUserSending := domain.UserSendings{
		UserId:    1,
		UserEmail: "test1@test.test",
		Sendings: []domain.UserSending{
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				1,
				1,
				"test1@test.test",
				1,
				"demo",
				5,
				time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				1,
				1,
				"test1@test.test",
				2,
				"demo",
				6,
				time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
		},
	}

	UserSendingAssembler(&userSending, countedUsers, "91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce")

	if false == reflect.DeepEqual(expectedUserSending, userSending) {
		t.Errorf("Test execute when one product failed, expected %+v, got %+v", expectedUserSending, userSending)
	}
}
