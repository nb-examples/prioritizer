package dto

import (
	"prioritizer/prioritizer/domain"
	"time"
)

type CountedUser struct {
	UserId      domain.UserId      `gorm:"column:user_id"`
	Email       domain.Email       `gorm:"column:email"`
	SchedulerId domain.ReleaseId   `gorm:"column:scheduler_id"`
	SegmentId   domain.SegmentId   `gorm:"column:segment_id"`
	Type        domain.SendingType `gorm:"column:type"`
	ProductId   domain.ProductId   `gorm:"column:product_id"`
	SendingTime time.Time          `gorm:"column:send_date"`
}

