package repository

import (
	"fmt"
	"github.com/avast/retry-go"
	"github.com/jinzhu/gorm"
	gormbulk "github.com/t-tiger/gorm-bulk-insert/v2"
	"gitlab.action-media.ru/golang/log"
	"prioritizer/db"
	"prioritizer/prioritizer/domain"
	"prioritizer/prioritizer/infrastructure/repository/assembler"
	"prioritizer/prioritizer/infrastructure/repository/dto"
	"sync"
	"time"
)

const TriggeredEmailTaskActivityType = 4

type UserSendingPostgresRepository struct {
	database             *db.DataBase
	maxCountBatch        int
	maxCountUsersOnBatch int
}

func NewUserSendingPostgresRepository(
	database *db.DataBase,
	maxCountBatch int,
	maxCountUsersOnBatch int,
) *UserSendingPostgresRepository {
	return &UserSendingPostgresRepository{
		database:             database,
		maxCountBatch:        maxCountBatch,
		maxCountUsersOnBatch: maxCountUsersOnBatch,
	}
}

func (repository *UserSendingPostgresRepository) Save(userSending *domain.UserSending) (err error) {
	err = repository.database.ConnectionPgSql.Save(userSending).Error
	return
}

func (repository *UserSendingPostgresRepository) GetUserSendingsBatches(job domain.Job) (<-chan domain.UsersSendingsBatch, <-chan error) {
	maxCountBatch := repository.maxCountBatch
	c := make(chan domain.UsersSendingsBatch, maxCountBatch)
	e := make(chan error, maxCountBatch)

	limit := repository.maxCountUsersOnBatch
	offset := 0

	go func() {
		defer close(c)
		defer close(e)

		var countGettingUsers int
		var countedUsersIds []int
		var lastCountedUserId int
		var countedUsers map[int][]dto.CountedUser
		var userLimits []domain.UserSendingLimit
		var userProductsCpk []domain.UserProductCpk
		var usersAllReadySent []domain.LimiterLog

		for {
			log.Info(fmt.Sprintf("Count getting users: %d", countGettingUsers))

			err := retry.Do(
				func() (err error) {
					countedUsersIds, err = repository.getCountedUsersIds(job.Day, lastCountedUserId, limit)
					log.Debug(fmt.Sprintf("Get counted users ids, last user id = %d", lastCountedUserId))

					lenCountedUsersIds := len(countedUsersIds)
					if lenCountedUsersIds > 0 {
						lastCountedUserId = countedUsersIds[lenCountedUsersIds-1]
					}
					countGettingUsers += lenCountedUsersIds
					return
				},
				retry.RetryIf(func(err error) bool {
					return len(countedUsersIds) != 0
				}),
				retry.OnRetry(func(n uint, err error) {
					log.Debug(fmt.Sprintf("Retry get counted users ids #%d: %s\n", n, err))
				}),
				retry.Attempts(3),
			)

			if len(countedUsersIds) == 0 {
				log.Info("All user sending processed")
				return
			}

			if nil != err {
				e <- err
				continue
			}

			err = retry.Do(
				func() (err error) {
					countedUsers, err = repository.getCountedUsersData(countedUsersIds)
					log.Debug(fmt.Sprintf("Get counted users, count %d", len(countedUsersIds)))
					return
				},
				retry.OnRetry(func(n uint, err error) {
					log.Debug(fmt.Sprintf("Retry get counted users #%d: %s\n", n, err))
				}),
				retry.Attempts(3),
			)
			if nil != err {
				e <- err
				continue
			}

			var wg sync.WaitGroup
			wg.Add(3)

			go func() {
				defer wg.Done()

				err = retry.Do(
					func() (err error) {
						userLimits, err = repository.getUserSendingLimit(countedUsersIds)
						return
					},
					retry.RetryIf(func(err error) bool {
						return len(userLimits) != 0
					}),
					retry.OnRetry(func(n uint, err error) {
						log.Debug(fmt.Sprintf("Retry get user sending limit #%d: %s\n", n, err))
					}),
					retry.Attempts(3),
				)
				if nil != err {
					e <- err
				}
			}()

			go func() {
				defer wg.Done()
				err = retry.Do(
					func() (err error) {
						userProductsCpk, err = repository.getUserProductCpk(countedUsersIds)
						return
					},
					retry.RetryIf(func(err error) bool {
						return len(userProductsCpk) != 0
					}),
					retry.OnRetry(func(n uint, err error) {
						log.Debug(fmt.Sprintf("Retry get user product cpk #%d: %s\n", n, err))
					}),
					retry.Attempts(3),
				)
				if nil != err {
					e <- err
				}
			}()

			go func() {
				defer wg.Done()
				userEmails := getAllEmails(countedUsers)

				err = retry.Do(
					func() (err error) {
						usersAllReadySent, err = repository.getReleaseLimitsByEmail(job.Day, userEmails)
						return
					},
					retry.RetryIf(func(err error) bool {
						return len(usersAllReadySent) != 0
					}),
					retry.OnRetry(func(n uint, err error) {
						log.Debug(fmt.Sprintf("Retry get release limits by email #%d: %s\n", n, err))
					}),
					retry.Attempts(3),
				)
				if nil != err {
					e <- err
				}
			}()

			wg.Wait()

			sendingsBatch := domain.NewUsersSendingsBatch()

			for _, userId := range countedUsersIds {
				var userSending domain.UserSendings

				userSending.UserId = domain.UserId(userId)
				userSending.UserEmail = countedUsers[userId][0].Email

				assembler.UserSendingAssembler(&userSending, countedUsers, job.Id)
				assembler.UserSendingLimitAssembler(&userSending, userLimits)
				assembler.UserProductCpkAssembler(&userSending, userProductsCpk)
				assembler.UserAllReadySentAssembler(&userSending, usersAllReadySent)

				sendingsBatch.Add(userSending)
			}

			log.Debug(fmt.Sprintf("Send users sendings batch to channel, count %d", len(sendingsBatch.GetUsers())))

			c <- *sendingsBatch

			offset += limit
		}
	}()

	return c, nil
}

func (repository *UserSendingPostgresRepository) getUserSendingLimit(userIds []int) (userLimits []domain.UserSendingLimit, err error) {
	err = repository.database.SegSegmentatorConnectionSql.
		Where("user_id IN (?)", userIds).
		Find(&userLimits).
		Error

	return
}

func (repository *UserSendingPostgresRepository) getUserProductCpk(userIds []int) (userProductsCpk []domain.UserProductCpk, err error) {
	err = repository.database.SegSegmentatorConnectionSql.
		Raw("select user_id, main_product, sum_mark from cpk where user_id IN (?) and type_mark = 'demo_mark'", userIds).
		Scan(&userProductsCpk).
		Error

	return
}

func (repository *UserSendingPostgresRepository) getCountedUsersIds(
	day time.Time,
	lastCountedUserId int,
	limit int,
) (countedUsersIds []int, err error) {
	err = repository.
		// если сильно упадет скорость, то докинуть индекс c учетом type_send, send_date и user_id
		database.SegWebpannelConnectionDB.
		Raw("select distinct user_id from counted_segment_user where type_send = 'email_subscription_release' and send_date::date >= ?::timestamp and user_id > ? order by user_id limit ?;", day.Format("2006-01-02"), lastCountedUserId, limit).
		Pluck("user_id", &countedUsersIds).
		Error

	return
}

func (repository *UserSendingPostgresRepository) getCountedUsersData(userIds []int) (map[int][]dto.CountedUser, error) {
	var countedUsers []dto.CountedUser

	mappedCountedUsers := make(map[int][]dto.CountedUser)

	err := repository.
		database.SegWebpannelConnectionDB.
		Raw("select * from counted_segment_user where user_id IN (?);", userIds).
		Scan(&countedUsers).
		Error
	if err != nil {
		return mappedCountedUsers, err
	}

	for _, countedUser := range countedUsers {
		userId := int(countedUser.UserId)
		mappedCountedUsers[userId] = append(mappedCountedUsers[userId], countedUser)
	}

	return mappedCountedUsers, err
}

func (repository *UserSendingPostgresRepository) getReleaseLimitsByEmail(day time.Time, emails []string) (userAllReadySent []domain.LimiterLog, err error) {
	dayFormat := day.Format("2006-01-02")

	err = repository.
		database.SegWebpannelConnectionDB.
		Raw(`
			select
			  ll.email,
			  coalesce(COUNT(ll.email) FILTER (WHERE type = 'market' and send_dt >= (?::date - interval '7 days')::date), 0) as market_count,
			  coalesce(COUNT(ll.email) FILTER (WHERE type = 'client' and send_dt >= (?::date - interval '7 days')::date), 0) as client_count,
              coalesce(COUNT(ll.email) FILTER (WHERE type = 'client' and send_dt = ?::date), 0) as client_count_for_today
			from limiter_log as ll
			join task t on ll.task_id = t.id
			where
			  ll.email IN (?)
			  and send_dt >= (?::date - interval '7 days')::date
			  and t."activityType" != ?
			  group by ll.email;
			`,
			dayFormat,
			dayFormat,
			dayFormat,
			emails,
			dayFormat,
			TriggeredEmailTaskActivityType,
		).
		Scan(&userAllReadySent).
		Error

	return
}

func (repository *UserSendingPostgresRepository) SaveMultiple(userSending []domain.UserSending) (err error) {
	err = repository.database.ConnectionPgSql.
		Create(userSending).
		Error

	return
}

func (repository *UserSendingPostgresRepository) GetAllUsersByReleaseId(releaseId domain.ReleaseId) (c chan domain.UserExportData, err error) {
	var userExportData domain.UserExportData

	limit := 10000
	offset := 0

	go func() {
		defer close(c)

		for {
			rows, err := repository.database.ConnectionPgSql.
				Raw("select distinct us.user_email from user_sendings us left join excluded_user_sendings eus on us.release_id = eus.release_id where eus.release_id is null and us.release_id = ? limit ? offset ?;", int(releaseId), limit, offset).
				Rows()

			if err != nil {
				return
			}

			for rows.Next() {
				_ = repository.database.ConnectionPgSql.ScanRows(rows, userExportData)
				c <- userExportData
			}

			_ = rows.Close()

			offset += limit
		}
	}()

	return c, nil
}

func (repository *UserSendingPostgresRepository) GetReleaseIdByJobId(jobId domain.JobId) (result []domain.ReleaseId, err error) {
	err = repository.database.ConnectionPgSql.
		Raw("select distinct release_id from user_sendings where job_id = ?;", string(jobId)).
		Scan(&result).
		Error

	return
}

func getAllEmails(usersIds map[int][]dto.CountedUser) (userEmails []string) {
	for _, userData := range usersIds {
		userEmails = append(userEmails, string(userData[0].Email))
	}

	return
}

type UserDeletedByPrioritizer struct {
	UserId                    int
	Email                     string
	SchedulerId               int
	SegmentId                 int
	SendDate                  time.Time
	ClientLimit               int
	MarketingLimit            int
	ClientEventsCount         int
	TodayClientEventsCount    int
	MarketingEventsCount      int
	TodayMarketingEventsCount int
}

func (udbp UserDeletedByPrioritizer) TableName() string {
	return "user_deleted_by_prioritizer"
}

func (repository *UserSendingPostgresRepository) SaveSendingsDeletedByPrioritizer(
	batch *domain.UsersSendingsBatch,
) (err error) {
	var udbp []interface{}
	for _, user := range batch.GetUsers() {
		for _, sending := range user.ExcludedSendings {
			udbp = append(udbp, UserDeletedByPrioritizer{
				UserId:               int(sending.UserId),
				Email:                string(sending.UserEmail),
				SchedulerId:          int(sending.ReleaseId),
				SegmentId:            int(sending.SegmentId),
				SendDate:             sending.SendingTime,
				ClientLimit:          user.UserSendingLimit.ClientLimit,
				MarketingLimit:       user.UserSendingLimit.MarketLimit,
				ClientEventsCount:    user.UserAllReadySent.ClientCount,
				MarketingEventsCount: user.UserAllReadySent.MarketCount,
			})
		}
	}

	err = gormbulk.BulkInsert(repository.database.SegWebpannelConnectionDB.DB, udbp, 100000)

	return
}

func (repository *UserSendingPostgresRepository) RemoveAllDeletedFromCounted(job domain.Job) (err error) {
	err = repository.database.SegWebpannelConnectionDB.
		Exec(`delete
                    from counted_segment_user c using user_deleted_by_prioritizer u
                    where c.user_id = u.user_id
                      and c.segment_id = u.segment_id
                      and c.scheduler_id = u.scheduler_id
                      and u.send_date::date = ?`, job.Day.Format("2006-01-02")).
		Error

	return
}

func (repository *UserSendingPostgresRepository) ExcludeUsersOfFirstShipmentFromSegments() (err error) {
	const typeOnceOnly = 1

	err = repository.database.SegWebpannelConnectionDB.
		Exec(`with schedulersWithFirstShipment as (

                       select id, "firstShipmentSchedulerId"
                       from scheduler
                       where id in (
                           select distinct scheduler_id from counted_segment_user
                       ) and type = ? and "firstShipmentSchedulerId" > 0

                    ), checkedFirstShipmentSchedules as (

                        select fs.id, swfs.id currentSchedulerId
                        from schedulersWithFirstShipment swfs
                        join scheduler fs on fs.id = swfs."firstShipmentSchedulerId"
                        where type = ? and "firstShipmentVolume" > 0

                    ), usersFromFirstShipments as (

                        select email, cfs.currentSchedulerId
                        from checkedFirstShipmentSchedules cfs
                        join limiter_log ll ON ll.schedule_id = cfs.id

                    )
                    delete from counted_segment_user cu
                    using usersFromFirstShipments ufs
                    where ufs.currentSchedulerId = cu.scheduler_id and ufs.email = cu.email`, typeOnceOnly, typeOnceOnly).
		Error

	return
}

func (repository *UserSendingPostgresRepository) MoveMultipleSendings() (err error) {
	const multipleSendingsType = 4

	err = repository.database.ConnectionPgSql.Transaction(func(tx *gorm.DB) (err error) {
		if err = repository.database.SegWebpannelConnectionDB.
			Exec(`insert into multiple_sending_user
                    select csu.user_id, csu.email, csu.segment_id, csu.product_id, csu.type, csu.send_date, csu.scheduler_id
                    from counted_segment_user csu
                    join scheduler s on csu.scheduler_id = s.id
                    where s.type = ?
                    on conflict do nothing;`, multipleSendingsType).
			Error; nil != err {
			return
		}

		err = repository.database.SegWebpannelConnectionDB.
			Exec(`delete from counted_segment_user csu
                    using scheduler s
                    where csu.scheduler_id = s.id and s.type = ?;`, multipleSendingsType).
			Error

		return
	})

	return
}

func (repository *UserSendingPostgresRepository) GetAbTestSchedulers() (result []domain.Scheduler, err error) {
	const TypeAbTest = 5

	err = repository.database.SegWebpannelConnectionDB.
		Raw(`
			SELECT DISTINCT "s".* 
			FROM "scheduler" "s" 
			JOIN "counted_segment_user" "csu" ON "csu".scheduler_id = "s"."parentId" 
			WHERE "s"."type" = ?;
		`, TypeAbTest).
		Scan(&result).
		Error

	return
}

func (repository *UserSendingPostgresRepository) GetTaskById(taskId int) (result domain.Task, err error) {
	err = repository.database.SegWebpannelConnectionDB.
		Raw(`
			SELECT "task".* 
			FROM "task"
			WHERE "task"."id" = ?
		`, taskId).
		First(&result).
		Error

	return
}

func (repository *UserSendingPostgresRepository) GetMapCountUsersByParentSchedulerIds(schedulerIds []int) (map[int]int, error) {
	var result []struct{
		Count int `gorm:"column:count"`
		SchedulerId int `gorm:"column:scheduler_id"`
	}

	err := repository.database.SegWebpannelConnectionDB.
		Raw(`
			SELECT COUNT(*) AS "count", "scheduler_id" 
			FROM "counted_segment_user" 
			WHERE "scheduler_id" IN (?)
			GROUP BY "scheduler_id";
		`, schedulerIds).
		Scan(&result).
		Error

	mapSlice := make(map[int]int)

	for _, row := range result {
		mapSlice[row.SchedulerId] = row.Count
	}

	return mapSlice, err
}

func (repository *UserSendingPostgresRepository) ReplaceSchedulerIdInCountedUsersWithLimit(parentSchedulerId, abSchedulerId, limit int) (err error) {
	err = repository.database.SegWebpannelConnectionDB.
		Exec(`
			UPDATE "counted_segment_user" 
			SET "scheduler_id" = ?
			WHERE "user_id" IN (
				SELECT "user_id"
				FROM "counted_segment_user" 
				WHERE "scheduler_id" = ?
				ORDER BY "user_id"
				LIMIT ?
			) AND "scheduler_id" = ?;
		`, abSchedulerId, parentSchedulerId, limit, parentSchedulerId).
		Error

	return
}
