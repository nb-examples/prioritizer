package aws

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"gitlab.action-media.ru/golang/log"
	"os"
	"path"
	"prioritizer/config"
	"prioritizer/prioritizer/application/adapters"
	"time"
)

type S3Client struct {
	awsSession *session.Session
	bucket     string
}

func NewS3Client(s3Connection *session.Session, cnf *config.Config) adapters.S3ClientInterface {
	return &S3Client{
		awsSession: s3Connection,
		bucket:     cnf.Aws.S3.Bucket,
	}
}

func (s3Client *S3Client) GetFileFromBucket(file *os.File, s3FilePath string) (err error) {
	downloader := s3manager.NewDownloader(s3Client.awsSession)

	_, err = downloader.Download(file, &s3.GetObjectInput{
		Bucket: aws.String(s3Client.bucket),
		Key:    aws.String(path.Base(s3FilePath)),
	})

	if err != nil {
		log.Error(err.Error())
	}

	return
}

func (s3Client *S3Client) PutFileToBucket(filePath string) {
	file, err := os.Open(filePath)

	if err != nil {
		log.Error(err.Error())
		return
	}

	defer file.Close()

	uploader := s3manager.NewUploader(s3Client.awsSession)

	_, err = uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(s3Client.bucket),
		Key:    aws.String(path.Base(filePath)),
		Body:   file,
	})

	if err != nil {
		log.Error(err.Error())
	}

	err = os.Remove(file.Name())

	if err != nil {
		log.Error(err.Error())
	}
}

func (s3Client *S3Client) GetFileUrlByKey(filePath string) (urlStr string) {
	svc := s3.New(s3Client.awsSession)

	req, _ := svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(s3Client.bucket),
		Key:    aws.String(path.Base(filePath)),
	})

	urlStr, err := req.Presign(24 * time.Hour)

	if err != nil {
		log.Error(err.Error())
	}

	return
}
