package aws

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"gitlab.action-media.ru/golang/log"
	"prioritizer/config"
)

func NewS3Connection(config *config.Config) *session.Session {
	awsConfig := config.Aws.S3

	sess, err := session.NewSession(&aws.Config{
		Endpoint:    aws.String(awsConfig.Host),
		Region:      aws.String(awsConfig.Region),
		Credentials: credentials.NewStaticCredentials(awsConfig.AccessKeyId, awsConfig.SecretAccessKey, ""),
	})

	if err != nil {
		log.Fatal(err.Error())
	}

	return sess
}
