package filesystem

import (
	"encoding/csv"
	"github.com/prometheus/common/log"
	"io"
	"os"
	"path"
	"prioritizer/prioritizer/application/adapters"
	"prioritizer/prioritizer/domain"
)

const TmpFileDir = "/tmp/"

func NewFileSystem() adapters.FileSystemInterface {
	return &FileSystem{}
}

type FileSystem struct {
}

func (fileSystem *FileSystem) ParseCsvFile(file *os.File) (chan domain.CsvFileData, error) {
	result := make(chan domain.CsvFileData, 1000)
	defer close(result)

	reader := csv.NewReader(file)

	go func() {
		for {
			line, err := reader.Read()
			if err != nil && err != io.EOF {
				log.Error(err.Error())
				return
			}

			result <- domain.CsvFileData{
				UserId: line[0],
				Email:  line[1],
			}
		}
	}()

	return result, nil
}

func (fileSystem *FileSystem) CreateFileIntoTmpDir(fileName string) (*os.File, error) {
	file, err := os.Create(TmpFileDir + fileName)
	return file, err
}

func (fileSystem *FileSystem) RemoveFile(file *os.File) (err error) {
	err = os.Remove(file.Name())
	return
}

func (fileSystem *FileSystem) RemoveFileByPath(filePath string) (err error) {
	err = os.Remove(filePath)
	return
}

func (fileSystem *FileSystem) GetBaseName(filePath string) string {
	return path.Base(filePath)
}

func (fileSystem *FileSystem) WriteToCsvFile(name string, userExportData <-chan domain.UserExportData) error {
	file, err := os.Create(name)
	if err != nil {
		return err
	}

	writer := csv.NewWriter(file)

	for row := range userExportData {
		line := []string{row.UserEmail}

		err = writer.Write(line)
		if err != nil {
			return err
		}
	}

	writer.Flush()

	return nil
}
