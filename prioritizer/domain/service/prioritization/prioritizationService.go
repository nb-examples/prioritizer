package prioritization

import (
	"context"
	"errors"
	"fmt"
	"github.com/avast/retry-go"
	"gitlab.action-media.ru/golang/log"
	"math"
	"prioritizer/prioritizer/application/service/prioritizationresult"
	"prioritizer/prioritizer/domain"
	"sync"
	"time"
)

type PrioritizationService struct {
	jobsRepository                 domain.JobRepositoryInterface
	userSendingsRepository         domain.UserSendingRepositoryInterface
	excludedUserSendingsRepository domain.ExcludedUserSendingRepositoryInterface
	handlerFactory                 HandlerFactoryInterface
	uploadToStorageService         *prioritizationresult.UploadToStorageService
	batchesGoroutinesCount         int
	singleUserGoroutinesCount      int
	deletedUsersToSaveBatchCount   int
}

func NewPrioritizationService(
	jobsRepository domain.JobRepositoryInterface,
	userSendingsRepository domain.UserSendingRepositoryInterface,
	excludedUserSendingsRepository domain.ExcludedUserSendingRepositoryInterface,
	handlerFactory HandlerFactoryInterface,
	uploadToStorageService *prioritizationresult.UploadToStorageService,
	batchesGoroutinesCount int,
	singleUserGoroutinesCount int,
	deletedUsersToSaveBatchCount int,
) PrioritizationServiceInterface {
	return &PrioritizationService{
		jobsRepository:                 jobsRepository,
		userSendingsRepository:         userSendingsRepository,
		excludedUserSendingsRepository: excludedUserSendingsRepository,
		handlerFactory:                 handlerFactory,
		uploadToStorageService:         uploadToStorageService,
		batchesGoroutinesCount:         batchesGoroutinesCount,
		singleUserGoroutinesCount:      singleUserGoroutinesCount,
		deletedUsersToSaveBatchCount:   deletedUsersToSaveBatchCount,
	}
}

func (service *PrioritizationService) Prioritize(job domain.Job) {
	service.markJobInProgress(&job)

	var wg sync.WaitGroup
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	if err := service.ExcludeUsersOfFirstShipmentFromSegments(); nil != err {
		service.markJobFailed(&job)
		return
	}

	usersBatchCh, errorsCh := service.userSendingsRepository.GetUserSendingsBatches(job)
	service.handleErrorFromChan(errorsCh, &job)

	singleUserCh := make(chan domain.UserSendings, service.singleUserGoroutinesCount)
	singleUserErrCh := make(chan error)
	defer close(singleUserErrCh)
	service.handleErrorFromChan(singleUserErrCh, &job)

	service.launchBatchesGoroutines(usersBatchCh, singleUserCh, &wg, ctx)

	service.launchSingleUserGoroutines(singleUserCh, singleUserErrCh, &job, &wg, ctx)

	wg.Wait()

	if err := service.RemoveDeletedUsersFromSendings(job); nil != err {
		service.markJobFailed(&job)
		return
	}

	if err := service.MoveMultipleSendings(); nil != err {
		service.markJobFailed(&job)
		return
	}

	if err := service.AbTestChangeUsersScheduler(); nil != err {
		service.markJobFailed(&job)
	} else {
		service.markJobFinished(&job, ctx)
	}
}

func (service *PrioritizationService) launchBatchesGoroutines(
	batchCh <-chan domain.UsersSendingsBatch,
	singleUserCh chan<- domain.UserSendings,
	wg *sync.WaitGroup,
	ctx context.Context,
) {
	/*
		в отдельной горутине, т.к. нужно дождаться конца выполнения всех batchesGoroutines
		и закрыть singleUserCh канал, при этом не блокируя поток вызова
	*/
	go func() {
		defer close(singleUserCh)

		var batchesWg sync.WaitGroup
		for i := 0; i < service.batchesGoroutinesCount; i++ {
			wg.Add(1)
			batchesWg.Add(1)

			go func() {
				defer wg.Done()
				defer batchesWg.Done()

				for userSendingBatch := range batchCh {
					select {
					case <-ctx.Done():
						return
					default:
					}

					log.Debug(fmt.Sprintf("Read users sendings batch from channel, count %d", len(userSendingBatch.GetUsers())))

					for _, userSendings := range userSendingBatch.GetUsers() {
						singleUserCh <- userSendings
					}
				}
			}()
		}

		batchesWg.Wait()
	}()
}

func (service *PrioritizationService) launchSingleUserGoroutines(
	singleUserCh <-chan domain.UserSendings,
	singleUserErrCh chan<- error,
	job *domain.Job,
	wg *sync.WaitGroup,
	ctx context.Context,
) {
	for i := 0; i < service.singleUserGoroutinesCount; i++ {
		wg.Add(1)

		go func() {
			defer wg.Done()

			usb := domain.NewUsersSendingsBatch()
			for userSendings := range singleUserCh {
				select {
				case <-ctx.Done():
					return
				default:
				}

				err := service.handlerFactory.GetHandler().Execute(&userSendings)
				if nil != err {
					singleUserErrCh <- err
					continue
				}

				if len(userSendings.ExcludedSendings) > 0 {
					usb.Add(userSendings)
				}

				if (len(usb.GetUsers())) >= service.deletedUsersToSaveBatchCount {
					service.saveDeletedSendings(usb, singleUserErrCh)
					log.Debug("Saved deleted sendings")
					usb = domain.NewUsersSendingsBatch()
				}

				job.IncrementUsersCount()
				job.IncrementSendingsCount(len(userSendings.Sendings) + len(userSendings.ExcludedSendings))
				job.IncrementDeletedSendingsCount(len(userSendings.ExcludedSendings))
			}
		}()
	}
}

func (service *PrioritizationService) markJobInProgress(job *domain.Job) {
	job.MarkInProgress()
	err := service.jobsRepository.Save(job)
	if nil != err {
		log.Error(err.Error())
	}
}

func (service *PrioritizationService) markJobFinished(job *domain.Job, ctx context.Context) {
	if errors.Is(ctx.Err(), context.Canceled) {
		job.MarkFailed()
	} else {
		job.MarkFinished()
	}

	err := service.jobsRepository.Save(job)
	if nil != err {
		log.Error(err.Error())
	}
}

func (service *PrioritizationService) markJobFailed(job *domain.Job) {
	job.MarkFailed()
	err := service.jobsRepository.Save(job)
	if nil != err {
		log.Error(err.Error())
	}
}

func (service *PrioritizationService) handleErrorFromChan(errorsCh <-chan error, job *domain.Job) {
	go func() {
		for e := range errorsCh {
			log.Error(e.Error())
			job.MarkWithErrors()
			e = service.jobsRepository.Save(job)
			if nil != e {
				log.Error(e.Error())
			}
		}
	}()
}

func (service *PrioritizationService) saveDeletedSendings(batch *domain.UsersSendingsBatch, errorsCh chan<- error) {
	err := retry.Do(
		func() (err error) {
			err = service.userSendingsRepository.SaveSendingsDeletedByPrioritizer(batch)
			return
		},
		retry.OnRetry(func(n uint, err error) {
			log.Debug(fmt.Sprintf("Deleted sendings save retry #%d: %s\n", n, err))
		}),
		retry.Attempts(3),
	)
	if nil != err {
		errorsCh <- err
	}
}

func (service *PrioritizationService) RemoveDeletedUsersFromSendings(job domain.Job) (err error) {
	err = retry.Do(
		func() (err error) {
			err = service.userSendingsRepository.RemoveAllDeletedFromCounted(job)
			return
		},
		retry.OnRetry(func(n uint, err error) {
			log.Debug(fmt.Sprintf("Removing deleted users from sendings retry #%d: %s\n", n, err))
		}),
		retry.Attempts(3),
		retry.Delay(time.Minute),
	)
	return
}

func (service *PrioritizationService) MoveMultipleSendings() (err error) {
	err = retry.Do(
		func() (err error) {
			err = service.userSendingsRepository.MoveMultipleSendings()
			return
		},
		retry.OnRetry(func(n uint, err error) {
			log.Debug(fmt.Sprintf("Moving multiple sendings retry #%d: %s\n", n, err))
		}),
		retry.Attempts(3),
		retry.Delay(time.Minute),
	)
	return
}

func (service *PrioritizationService) ExcludeUsersOfFirstShipmentFromSegments() (err error) {
	err = retry.Do(
		func() (err error) {
			err = service.userSendingsRepository.ExcludeUsersOfFirstShipmentFromSegments()
			return
		},
		retry.OnRetry(func(n uint, err error) {
			log.Debug(fmt.Sprintf("Exclude users of first shipment from segments retry #%d: %s\n", n, err))
		}),
		retry.Attempts(3),
		retry.Delay(time.Minute),
	)
	return
}

func (service *PrioritizationService) AbTestChangeUsersScheduler() (err error) {
	var abTestSchedulers []domain.Scheduler

	err = retry.Do(
		func() (err error) {
			abTestSchedulers, err = service.userSendingsRepository.GetAbTestSchedulers()
			return
		},
		retry.OnRetry(func(n uint, err error) {
			log.Debug(fmt.Sprintf("Get ab test schedulers retry #%d: %s\n", n, err))
		}),
		retry.Attempts(3),
		retry.Delay(time.Minute),
	)
	if nil != err {
		return
	}

	if 0 == len(abTestSchedulers) {
		return
	}

	var abTestParentSchedulerIds []int

	for _, abTestScheduler := range abTestSchedulers {
		abTestParentSchedulerIds = append(abTestParentSchedulerIds, abTestScheduler.ParentId)
	}

	var mapCountUsers map[int]int

	err = retry.Do(
		func() (err error) {
			mapCountUsers, err = service.userSendingsRepository.GetMapCountUsersByParentSchedulerIds(abTestParentSchedulerIds)
			return
		},
		retry.OnRetry(func(n uint, err error) {
			log.Debug(fmt.Sprintf("Get map count users by parent scheduler ids retry #%d: %s\n", n, err))
		}),
		retry.Attempts(3),
		retry.Delay(time.Minute),
	)
	if nil != err {
		return
	}

	var task domain.Task

	for _, abTestScheduler := range abTestSchedulers {
		parentId := abTestScheduler.ParentId

		err = retry.Do(
			func() (err error) {
				task, err = service.userSendingsRepository.GetTaskById(abTestScheduler.TaskId)
				return
			},
			retry.OnRetry(func(n uint, err error) {
				log.Debug(fmt.Sprintf("Get task by id retry #%d: %s\n", n, err))
			}),
			retry.Attempts(3),
			retry.Delay(time.Minute),
		)
		if nil != err {
			return
		}

		limit := int(math.Ceil(float64(task.PercentOfUsers) / float64(100) * float64(mapCountUsers[parentId])))

		err = retry.Do(
			func() (err error) {
				err = service.userSendingsRepository.ReplaceSchedulerIdInCountedUsersWithLimit(
					parentId,
					abTestScheduler.Id,
					limit,
				)
				return
			},
			retry.OnRetry(func(n uint, err error) {
				log.Debug(fmt.Sprintf("Replace scheduler id in counted users with limit retry #%d: %s\n", n, err))
			}),
			retry.Attempts(3),
			retry.Delay(time.Minute),
		)
		if nil != err {
			return
		}
	}

	return
}
