package prioritization

import "prioritizer/prioritizer/domain"

type CalculateAvailableLimitsInterface interface {
	Invoke(log domain.LimiterLog, limit domain.UserSendingLimit) (availableLimitForUser AvailableLimitForUser)
}

func NewCalculateAvailableLimits() CalculateAvailableLimitsInterface {
	return &CalculateAvailableLimits{}
}

// Structure keeps number of release which we can send today
type AvailableLimitForUser struct {
	Market int
	Client int
}

type CalculateAvailableLimits struct {}

// If count of released mailing less than limit, return count of remaining
func (calculateAvailableLimits *CalculateAvailableLimits) Invoke(log domain.LimiterLog, limit domain.UserSendingLimit) (availableLimitForUser AvailableLimitForUser) {
	if limit.ClientLimit > log.ClientCount {
		availableLimitForUser.Client = limit.ClientLimit - log.ClientCount
	} else {
		if log.ClientCountForToday == 0 {
			availableLimitForUser.Client = 1
		} else {
			availableLimitForUser.Client = 0
		}
	}

	if limit.MarketLimit > log.MarketCount {
		availableLimitForUser.Market = limit.MarketLimit - log.MarketCount
	}

	return
}
