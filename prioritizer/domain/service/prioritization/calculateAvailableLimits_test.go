package prioritization

import (
	"prioritizer/prioritizer/domain"
	"reflect"
	"testing"
)

var calculationService = NewCalculateAvailableLimits()

func TestGetAvailableLimitsSuccess(t *testing.T) {
	log := domain.LimiterLog{
		Email:               "test@test.ru",
		MarketCount:         10,
		ClientCount:         3,
	}

	limit := domain.UserSendingLimit{UserId: 1, ClientLimit: 30, MarketLimit: 10}

	expectedResult := AvailableLimitForUser{Client: 27, Market: 0}

	result := calculationService.Invoke(log, limit)

	if false == reflect.DeepEqual(result, expectedResult) {
		t.Errorf("Test execute when one product failed, expected %+v, got %+v", expectedResult, result)
	}
}
