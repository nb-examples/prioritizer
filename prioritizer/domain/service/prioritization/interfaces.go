package prioritization

import "prioritizer/prioritizer/domain"

type HandlerInterface interface {
	Execute(userSendings *domain.UserSendings) error
}

type HandlerFactoryInterface interface {
	GetHandler() HandlerInterface
}

type PrioritizeRequestDto struct {
	JobId string
}

type PrioritizationServiceInterface interface {
	Prioritize(job domain.Job)
}

type CompositeHandler struct {
	handlers []HandlerInterface
}

func (composite *CompositeHandler) Add(handler HandlerInterface) {
	composite.handlers = append(composite.handlers, handler)
}

func (composite CompositeHandler) Execute(userSendings *domain.UserSendings) (err error) {
	for _, handler := range composite.handlers {
		err = handler.Execute(userSendings)
		if nil != err {
			return
		}
	}
	return
}
