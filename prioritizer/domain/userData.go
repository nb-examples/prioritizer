package domain

type UserSendingLimit struct {
	UserId      UserId `gorm:"column:user_id"`
	ClientLimit int    `gorm:"column:client_limit"`
	MarketLimit int    `gorm:"column:marketing_limit"`
}

type UserProductCpk struct {
	UserId    UserId    `gorm:"column:user_id"`
	ProductId ProductId `gorm:"column:main_product"`
	Value     int       `gorm:"column:sum_mark"`
}

type LimiterLog struct {
	Email               string `gorm:"column:email"`
	MarketCount         int    `gorm:"column:market_count"`
	ClientCount         int    `gorm:"column:client_count"`
	ClientCountForToday int    `gorm:"column:client_count_for_today"`
}

type UserExportData struct {
	UserEmail string
}

type CsvFileData struct {
	UserId string
	Email  string
}

func (upcpk UserProductCpk) TableName() string {
	return "user_product_cpk"
}

func (userSendingLimit UserSendingLimit) TableName() string {
	return "user_email_limits"
}

type Scheduler struct {
	Id       int `gorm:"column:id"`
	ParentId int `gorm:"column:parentId"`
	TaskId   int `gorm:"column:taskId"`
}

type Task struct {
	Id             int `gorm:"column:id"`
	PercentOfUsers int `gorm:"column:percentOfUsers"`
}

func (task Task) TableName() string {
	return "task"
}
