package domain

import "time"

type UserSendingId string
type ReleaseId int
type UserId int
type SegmentId int
type SendingType string
type ProductId int
type Email string

const (
	MARKETING SendingType = "market"
	DEMO                  = "demo"
	ADV                   = "adv"
	CLIENT                = "client"
	NEWS                  = "news"
	PROJECT               = "project"
	EVENT                 = "event"
)

func (sendingType SendingType) IsMarketing() bool {
	return sendingType != CLIENT
}

func (sendingType SendingType) IsClient() bool {
	return sendingType == CLIENT
}

type UserSending struct {
	JobId       JobId
	ReleaseId   ReleaseId
	UserId      UserId
	UserEmail   Email
	SegmentId   SegmentId
	Type        SendingType
	ProductId   ProductId
	SendingTime time.Time
}

type UserSendings struct {
	UserId           UserId
	UserEmail        Email
	Sendings         []UserSending
	UserSendingLimit UserSendingLimit
	UserProductsCpk  []UserProductCpk
	UserAllReadySent LimiterLog
	ExcludedSendings []UserSending
}

func (sendings *UserSendings) ExcludeSending(sendingKey int) {
	sendings.ExcludedSendings = append(sendings.ExcludedSendings, sendings.Sendings[sendingKey])
	sendings.Sendings = append(sendings.Sendings[:sendingKey], sendings.Sendings[sendingKey+1:]...)
}

type UsersSendingsBatch struct {
	users map[UserId]UserSendings
}

func NewUsersSendingsBatch() *UsersSendingsBatch {
	return &UsersSendingsBatch{make(map[UserId]UserSendings)}
}

func (batch *UsersSendingsBatch) Add(sendings UserSendings) {
	batch.users[sendings.UserId] = sendings
}

func (batch *UsersSendingsBatch) GetUsers() map[UserId]UserSendings {
	return batch.users
}

type UserSendingRepositoryInterface interface {
	Save(*UserSending) error
	GetUserSendingsBatches(Job) (<-chan UsersSendingsBatch, <-chan error)
	SaveMultiple([]UserSending) error
	GetReleaseIdByJobId(JobId) ([]ReleaseId, error)
	GetAllUsersByReleaseId(ReleaseId) (chan UserExportData, error)
	SaveSendingsDeletedByPrioritizer(*UsersSendingsBatch) error
	RemoveAllDeletedFromCounted(Job) error
	ExcludeUsersOfFirstShipmentFromSegments() error
	MoveMultipleSendings() error
	GetAbTestSchedulers() ([]Scheduler, error)
	GetTaskById(int) (Task, error)
	GetMapCountUsersByParentSchedulerIds(schedulerIds []int) (map[int]int, error)
	ReplaceSchedulerIdInCountedUsersWithLimit(parentSchedulerId, abSchedulerId, limit int) error
}

type ExcludedUserSendingRepositoryInterface interface {
	Save([]UserSending) error
}
