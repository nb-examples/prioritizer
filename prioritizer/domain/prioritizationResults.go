package domain

type SegmentUrl string

type PrioritizationResult struct {
	ReleaseId  ReleaseId
	JobId      JobId
	SegmentUrl SegmentUrl
}

type PrioritizationResultsRepositoryInterface interface {
	Save(*PrioritizationResult) error
	GetByJobId(JobId) ([]PrioritizationResult, error)
}
