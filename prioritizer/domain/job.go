package domain

import (
	"sync/atomic"
	"time"
)

type JobId string
type JobStatus string

const (
	New        JobStatus = "new"
	InProgress JobStatus = "in_progress"
	Finished   JobStatus = "finished"
	Failed     JobStatus = "failed"
	ReLaunched JobStatus = "re_launch"
)

type Job struct {
	Id                   JobId
	Status               JobStatus
	Day                  time.Time
	WithErrors           bool
	StartedAt            *time.Time
	EndedAt              *time.Time
	CreatedAt            time.Time
	UpdatedAt            time.Time
	UsersCount           int64
	SendingsCount        int64
	DeletedSendingsCount int64
}

func (job *Job) IsReadyToBePrioritized() bool {
	return New == job.Status ||
		ReLaunched == job.Status
}

func (job *Job) MarkFailed() {
	now := time.Now()
	job.Status = Failed
	job.EndedAt = &now
}

func (job *Job) MarkInProgress() {
	now := time.Now()
	job.Status = InProgress
	job.StartedAt = &now
}

func (job *Job) MarkFinished() {
	now := time.Now()
	job.Status = Finished
	job.EndedAt = &now
}

func (job *Job) MarkReLaunched() {
	job.Status = ReLaunched
}

func (job *Job) IsFailed() bool {
	return job.Status == Failed
}

func (job *Job) MarkWithErrors() {
	job.WithErrors = true
}

func (job *Job) IncrementUsersCount() {
	atomic.AddInt64(&job.UsersCount, 1)
}

func (job *Job) IncrementSendingsCount(count int) {
	atomic.AddInt64(&job.SendingsCount, int64(count))
}

func (job *Job) IncrementDeletedSendingsCount(count int) {
	atomic.AddInt64(&job.DeletedSendingsCount, int64(count))
}

type JobRepositoryInterface interface {
	GetNextIdentity() JobId
	GetById(jobId JobId) (*Job, error)
	GetByDay(day time.Time) (*Job, error)
	GetByStatus(status JobStatus) ([]Job, error)
	Save(job *Job) error
}
