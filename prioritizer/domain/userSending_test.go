package domain

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestUserSendings_ExcludeSending(t *testing.T) {
	firstId := "91ea5e08-aa85-487c-8d7b-ffcb4b9b1fce"
	sendings := UserSendings{
		UserId:    123,
		UserEmail: "a@a.a",
		Sendings: []UserSending{
			{
				JobId(firstId),
				1,
				123,
				"a@a.a",
				1,
				"market",
				5,
				time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
			{
				"11ea5e08-aa85-487c-8d7b-ffcb4b9b1fce",
				1,
				123,
				"a@a.a",
				1,
				"client",
				5,
				time.Date(2020, 9, 2, 00, 00, 00, 651387237, time.UTC),
			},
		},
		UserSendingLimit: UserSendingLimit{},
		UserProductsCpk:  nil,
		UserAllReadySent: LimiterLog{},
		ExcludedSendings: nil,
	}

	sendings.ExcludeSending(1)

	assert.Len(t, sendings.Sendings, 1)
	assert.Len(t, sendings.ExcludedSendings, 1)
	assert.Equal(t, firstId, string(sendings.Sendings[0].JobId))
}
