package ui

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	"gitlab.action-media.ru/golang/log"
	"prioritizer/prioritizer/application/service/segmentdownloader"
	"prioritizer/prioritizer/application/service/segmentdownloader/dto"
	"time"
)

func NewReleaseController(segmentDownloader *segmentdownloader.SegmentDownloader) *ReleaseController {
	return &ReleaseController{
		segmentDownloader: segmentDownloader,
	}
}

type ReleaseController struct {
	segmentDownloader *segmentdownloader.SegmentDownloader
}

// swagger:parameters addSegmentToJobRequest
type AddSegmentToJobRequest struct {
	// In: body
	// Example: 75421
	// Required: true
	UserSendingId string `json:"userSendingId"`
	//
	// In: body
	// Example: 123e4567-e89b-12d3-a456-426655440000
	// Required: true
	JobId string `json:"jobId"`
	//
	// In: body
	// Example: 123
	// Required: true
	ReleaseId string `json:"releaseId"`
	//
	// In: body
	// Example: 456
	// Required: true
	SegmentId int `json:"segmentId"`
	//
	// In: body
	// Example: market
	// Required: true
	Type string `json:"type"`
	//
	// In: body
	// Example: 111
	// Required: true
	ProductId int `json:"productId"`
	//
	// In: body
	// Example: 2020-10-02 12:00:00
	// Required: true
	SendingTime time.Time `json:"sendingTime"`
	//
	// In: body
	// Example: s3://test.ru/123_job_456_release.csv
	// Required: true
	FilePath string `json:"filePath"`
}

// swagger:route POST /api/v1/release_save-release-to-job addSegmentToJobRequest
//
// Метод для добавления выпуска к джобе
//
// Produces:
// 	 - application/json
//
// Response:
//	 200: description: Success
//	 400: description:Bad Request
//	 500: description:Internal Server Error
func (controller *ReleaseController) SaveReleaseToJob(context *fasthttp.RequestCtx) {
	var segmentToJob dto.SegmentToJob

	request := context.PostArgs().Peek("addSegmentToJobRequest")

	err := json.Unmarshal(request, &segmentToJob)
	if err != nil {
		log.Error(err.Error())
		context.Error("bad params", fasthttp.StatusBadRequest)
		return
	}

	controller.segmentDownloader.Invoke(segmentToJob)

	context.SetContentType("application/json; charset=utf-8")
	context.SetStatusCode(fasthttp.StatusOK)
}
