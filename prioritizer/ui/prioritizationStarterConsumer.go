package ui

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"gitlab.action-media.ru/golang/log"
	"prioritizer/prioritizer/application/service/prioritization"
	"prioritizer/rabbitmq"
	"time"
)

type PrioritizationStarterConsumer struct {
	rabbitMq *rabbitmq.RabbitMQ
	launcher *prioritization.PrioritizationLauncher
}

func NewPrioritizationStarterConsumer(
	rabbitMq *rabbitmq.RabbitMQ,
	launcher *prioritization.PrioritizationLauncher,
) *PrioritizationStarterConsumer {
	return &PrioritizationStarterConsumer{rabbitMq: rabbitMq, launcher: launcher}
}

type StartPrioritizationEvent struct {
	Data     StartPrioritizationEventData     `json:"data"`
	Metadata StartPrioritizationEventMetaData `json:"metadata"`
}

type StartPrioritizationEventData struct {
	Day string `json:"day"`
}

type StartPrioritizationEventMetaData struct {
	CreatedAt string `json:"createdAt"`
}

const (
	PrioritizationStarterExchangeName = "priority_process_start_needed"
	PrioritizationStarterExchangeType = "fanout"
	PrioritizationStarterQueueName    = "priority_process_start_needed_start_prioritization"
)

func (consumer *PrioritizationStarterConsumer) Consume() (err error) {
	log.Info("PrioritizationStarterConsumer is running")

	connection := consumer.rabbitMq.ConnectionRabbitMQ

	channel, err := connection.Channel()
	if err != nil {
		log.Error(err.Error())
		return
	}

	err = channel.ExchangeDeclare(
		PrioritizationStarterExchangeName,
		PrioritizationStarterExchangeType,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Error(err.Error())
		return
	}

	queue, err := channel.QueueDeclare(
		PrioritizationStarterQueueName,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Error(err.Error())
		return
	}

	err = channel.QueueBind(
		queue.Name,
		"",
		PrioritizationStarterExchangeName,
		false,
		nil,
	)
	if err != nil {
		log.Error(err.Error())
		return
	}

	messages, err := channel.Consume(
		PrioritizationStarterQueueName,
		"",
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Error(err.Error())
		return
	}

	var event StartPrioritizationEvent

	go func() {
		for message := range messages {
			log.Info("message received: " + string(message.Body))

			if err := json.Unmarshal(message.Body, &event); err != nil {
				log.Error(err.Error())
				AckMessage(message)
				continue
			}

			day, err := time.Parse("2006-01-02", event.Data.Day)
			if err != nil {
				log.Error(err.Error())
				AckMessage(message)
				continue
			}

			err = consumer.launcher.LaunchPrioritization(prioritization.PrioritizeRequestDto{
				Day: day,
			})
			if err != nil {
				log.Error(err.Error())
				continue
			}

			AckMessage(message)
		}
	}()

	return
}

func AckMessage(message amqp.Delivery) {
	if err := message.Ack(false); err != nil {
		log.Error(err.Error())
	}
}
