package ui

import (
	"encoding/json"
	"errors"
	"github.com/jinzhu/gorm"
	"github.com/valyala/fasthttp"
	"gitlab.action-media.ru/golang/log"
	jobService "prioritizer/prioritizer/application/job"
	"prioritizer/prioritizer/application/service/prioritization"
	"prioritizer/prioritizer/domain"
	"time"
)

type JobController struct {
	jobService             *jobService.JobService
	prioritizationLauncher *prioritization.PrioritizationLauncher
}

func NewJobController(jobService *jobService.JobService) *JobController {
	return &JobController{jobService: jobService}
}

// swagger:parameters jobId
type JobOneRequest struct {
	// Job Id
	//
	// Required: true
	// In: query
	Id string `json:"id"`
}

type JobOneResponse struct {
	// Id
	Id string `json:"id"`

	// Статус
	// Example: new
	Status string `json:"status"`

	// С ошибками
	WithErrors bool `json:"withErrors"`

	// День
	Day time.Time `json:"day"`

	// Дата начала
	StartedAt *time.Time `json:"startedAt"`

	// Дата конца
	EndedAt *time.Time `json:"endedAt"`

	// Количество пользователей
	UsersCount int64 `json:"usersCount"`

	// Количество отправок
	SendingsCount int64 `json:"sendingsCount"`

	// Количество удаленных отправок
	DeletedSendingsCount int64 `json:"deletedSendingsCount"`
}

// Успешный ответ
// swagger:response SuccessJobOneResponse
type SuccessJobOneResponse struct {
	// In: body
	Body JobOneResponse
}

// swagger:route GET /api/v1/job_get-by-id job jobId
// Получить job по id
//
// _
// Produces:
//   - application/json
// Responses:
//   200: SuccessJobOneResponse
//   400: description:Bad Request
//   404: description:Job not found
//   500: description:Internal Server Error
func (controller JobController) GetById(context *fasthttp.RequestCtx) {
	id := string(context.QueryArgs().Peek("id"))

	if id == "" {
		context.Error("id param required", fasthttp.StatusBadRequest)
		return
	}

	if !IsValidUUID(id) {
		context.Error("id must be uuid", fasthttp.StatusBadRequest)
		return
	}

	job, err := controller.jobService.GetById(domain.JobId(id))
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			context.Error("Job not found", fasthttp.StatusNotFound)
			return
		}

		log.Error(err.Error())
		context.Error("Error occurred while getting job by id", fasthttp.StatusInternalServerError)
		return
	}

	response, err := json.Marshal(JobOneResponse{
		Id: string(job.Id),
		Status: string(job.Status),
		WithErrors: job.WithErrors,
		Day: job.Day,
		StartedAt: job.StartedAt,
		EndedAt: job.EndedAt,
		UsersCount: job.UsersCount,
		SendingsCount: job.SendingsCount,
		DeletedSendingsCount: job.DeletedSendingsCount,
	})
	if err != nil {
		log.Error(err.Error())
		context.Error("Something went wrong", fasthttp.StatusInternalServerError)
		return
	}

	context.SetContentType("application/json; charset=utf-8")
	context.SetStatusCode(fasthttp.StatusOK)
	context.SetBody(response)
}

// swagger:route POST /api/v1/job_re-launch-by-id job jobId
// Перезапустить job по id
//
// _
// Produces:
//   - application/json
// Responses:
//   200: SuccessJobOneResponse
//   400: description:Bad Request
//   404: description:Job not found
//   500: description:Internal Server Error
func (controller JobController) ReLaunchById(context *fasthttp.RequestCtx) {
	id := string(context.PostArgs().Peek("id"))

	if id == "" {
		context.Error("id param required", fasthttp.StatusBadRequest)
		return
	}

	if !IsValidUUID(id) {
		context.Error("id must be uuid", fasthttp.StatusBadRequest)
		return
	}

	job, err := controller.jobService.GetById(domain.JobId(id))
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			context.Error("Job not found", fasthttp.StatusNotFound)
			return
		}

		log.Error(err.Error())
		context.Error("Error occurred while re-launch job by id", fasthttp.StatusInternalServerError)
		return
	}

	err = controller.jobService.ReLaunch(job)
	if err != nil {
		log.Error(err.Error())
		context.Error("Something went wrong while re-launch job", fasthttp.StatusInternalServerError)
		return
	}

	response, err := json.Marshal(JobOneResponse{
		Id: string(job.Id),
		Status: string(job.Status),
		WithErrors: job.WithErrors,
		Day: job.Day,
		StartedAt: job.StartedAt,
		EndedAt: job.EndedAt,
		UsersCount: job.UsersCount,
		SendingsCount: job.SendingsCount,
		DeletedSendingsCount: job.DeletedSendingsCount,
	})

	if err != nil {
		log.Error(err.Error())
		context.Error("Something went wrong", fasthttp.StatusInternalServerError)
		return
	}

	context.SetContentType("application/json; charset=utf-8")
	context.SetStatusCode(fasthttp.StatusOK)
	context.SetBody(response)
}
