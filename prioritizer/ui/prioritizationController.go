package ui

import (
	"github.com/valyala/fasthttp"
	"gitlab.action-media.ru/golang/log"
	jobService "prioritizer/prioritizer/application/job"
	"prioritizer/prioritizer/application/service/prioritization"
	"time"
)

func NewPrioritizationController(
	jobService *jobService.JobService,
	launcher *prioritization.PrioritizationLauncher,
) *PrioritizationController {
	return &PrioritizationController{
		launcher:   launcher,
		jobService: jobService,
	}
}

// swagger:parameters prioritizationStartRequest
type PrioritizationStartRequest struct {
	// Day
	//
	// Required: true
	// In: body
	Day string `json:"day"`
}

type PrioritizationController struct {
	jobService *jobService.JobService
	launcher   *prioritization.PrioritizationLauncher
}

// swagger:route POST /api/v1/prioritization_start prioritizationStartRequest
// Запуск приоритизации
//
// _
// Produces:
//   - application/json
// Responses:
//   200: description: Success
//   400: description:Bad Request
//   404: description:Job not found
//   500: description:Internal Server Error
func (controller *PrioritizationController) Start(context *fasthttp.RequestCtx) {
	dayRequest := string(context.PostArgs().Peek("day"))

	if dayRequest == "" {
		context.Error("day param required", fasthttp.StatusBadRequest)
		return
	}

	day, err := time.Parse("2006-01-02", dayRequest)
	if err != nil {
		context.Error("day param invalid", fasthttp.StatusBadRequest)
		return
	}

	prioritizeRequestDto := prioritization.PrioritizeRequestDto{
		Day: day,
	}

	err = controller.launcher.LaunchPrioritization(prioritizeRequestDto)

	if err != nil {
		log.Error(err.Error())
		context.Error("Error occurred while start prioritization", fasthttp.StatusInternalServerError)
		return
	}

	context.SetContentType("application/json; charset=utf-8")
	context.SetStatusCode(fasthttp.StatusOK)
}
