package ui

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	"gitlab.action-media.ru/golang/log"
	"prioritizer/prioritizer/application/service/prioritizationresult"
)

type PrioritizationResultController struct {
	getPrioritizationResultsService *prioritizationresult.GetPrioritizationResultsService
}

func NewPrioritizationResultController(
	getPrioritizationResultsService *prioritizationresult.GetPrioritizationResultsService,
) *PrioritizationResultController {
	return &PrioritizationResultController{
		getPrioritizationResultsService: getPrioritizationResultsService,
	}
}

// swagger:parameters GetByJobId
type JobPrioritizationResultsRequest struct {
	// Job Id
	//
	// Required: true
	// In: query
	JobId string `json:"job_id"`
}

// Успешный ответ
// swagger:response JobPrioritizationResultsResponse
type JobPrioritizationResultsResponse struct {
	// In: body
	Body JobPrioritizationResultsResponseBody
}

type JobPrioritizationResultsResponseBody struct {
	// Сообщение
	// Example: success
	Message string `json:"message"`
	// Набор выпусков и url приоритизированных сегментов
	Data map[int]string `json:"data"`
}

// swagger:route GET /api/v1/prioritization-result_get-by-job-id prioritization-result GetByJobId
// Получить результаты приоритезации по job id
//
// _
// Produces:
//   - application/json
// Responses:
//   200: JobPrioritizationResultsResponse
//   400: description:Bad Request
//   500: description:Internal Server Error
func (controller *PrioritizationResultController) GetByJobId(context *fasthttp.RequestCtx) {
	jobId := string(context.QueryArgs().Peek("job_id"))

	if jobId == "" {
		context.Error("job_id param required", fasthttp.StatusBadRequest)
		return
	}

	if ! IsValidUUID(jobId) {
		context.Error("job_id must be uuid", fasthttp.StatusBadRequest)
		return
	}

	results, err := controller.getPrioritizationResultsService.GetByJobId(jobId)
	if nil != err {
		log.Error(err.Error())
		context.Error("Something went wrong", fasthttp.StatusInternalServerError)
		return
	}

	var response JobPrioritizationResultsResponseBody
	response.Data = make(map[int]string)
	for _, result := range results {
		response.Data[int(result.ReleaseId)] = string(result.SegmentUrl)
	}
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		log.Error(err.Error())
		context.Error("Something went wrong", fasthttp.StatusInternalServerError)
		return
	}

	context.SetContentType("application/jsonResponse; charset=utf-8")
	context.SetStatusCode(fasthttp.StatusOK)
	context.SetBody(jsonResponse)
}
