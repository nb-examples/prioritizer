// Package ui Prioritizer API.
//
// Version: 1.0.0
//
// swagger:meta
package ui

import uuid "github.com/satori/go.uuid"

func IsValidUUID(u string) bool {
	_, err := uuid.FromString(u)
	return err == nil
}
