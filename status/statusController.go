package status

import (
	"encoding/json"
	"prioritizer/config"
	"os"

	"github.com/valyala/fasthttp"
)

type StatusController struct {
	config *config.Config
}

type StatusResponse struct {

	// Названия продукта
	// Example: seg
	ProductName string `json:"product_name"`

	// Названия сервиса
	// Example: prioritizer
	ServiceName string `json:"service_name"`

	// Тип приложения
	// Example: webapp
	AppType string `json:"app_type"`

	// Окружение в котором работает приложение
	// Example: prod
	Env string `json:"environment"`
}

// Успешный ответ
// swagger:response SuccessStatusResponse
type SuccessStatusResponse struct {
	// In: body
	Body StatusResponse
}

func NewStatusController(config *config.Config) *StatusController {
	return &StatusController{config: config}
}

// swagger:route GET /_/status default getStatus
// Статус приложения
//
// _
// Produces:
//   - application/json
// Responses:
//   200: SuccessStatusResponse
//   500: description:Internal Server Error
func (controller *StatusController) GetStatus(context *fasthttp.RequestCtx) {
	response, err := json.Marshal(StatusResponse{
		ProductName: os.Getenv("PRODUCT_NAME"),
		ServiceName: os.Getenv("SERVICE_NAME"),
		AppType:     string(controller.config.AppType),
		Env:         os.Getenv("ENVIRONMENT"),
	})
	if err != nil {
		context.Error(err.Error(), fasthttp.StatusInternalServerError)
		return
	}
	context.Write(response)
	context.SetContentType("application/json; charset=utf-8")
	context.SetStatusCode(fasthttp.StatusOK)
}
