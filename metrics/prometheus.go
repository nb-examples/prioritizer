package metrics

import "github.com/prometheus/client_golang/prometheus"

type Prometheus struct {
	RequestsTotal    *prometheus.CounterVec
	RequestsDuration *prometheus.HistogramVec
}

func NewPrometheus(subsystem string, hostName string) *Prometheus {
	prom := &Prometheus{
		RequestsTotal: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Subsystem:   subsystem,
				Name:        "requests_total",
				ConstLabels: map[string]string{"hostName": hostName},
			},
			[]string{"code", "method", "host", "url"},
		),
		RequestsDuration: prometheus.NewHistogramVec(
			prometheus.HistogramOpts{
				Subsystem:   subsystem,
				Name:        "request_duration_seconds",
				Buckets:     []float64{0.0001, 0.0005, 0.001, 0.0025, 0.005, 0.01, 0.025, 0.05, 0.1, 0.25, 0.5, 1, 2.5, 5},
				ConstLabels: map[string]string{"hostName": hostName},
			},
			[]string{"code", "method", "host", "url"},
		),
	}
	prometheus.MustRegister(
		prom.RequestsTotal,
		prom.RequestsDuration,
	)
	return prom
}
