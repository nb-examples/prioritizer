package metrics

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttpadaptor"
)

type MetricsController struct {
}

func NewMetricsController() *MetricsController {
	return &MetricsController{}
}

// swagger:route GET /_/metrics default getMetrics
// Метрики в формате Prometheus
//
// _
// Responses:
//   200: description:Успешный ответ
func (controller *MetricsController) GetMetrics(context *fasthttp.RequestCtx) {
	handler := fasthttpadaptor.NewFastHTTPHandler(promhttp.Handler())
	handler(context)
}
