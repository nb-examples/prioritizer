package main

import (
	"fmt"
	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
	"gitlab.action-media.ru/golang/log"
	"go.uber.org/dig"
	"go.uber.org/multierr"
	"os"
	"os/signal"
	"prioritizer/config"
	"prioritizer/db"
	"prioritizer/metrics"
	"prioritizer/middleware"
	"prioritizer/prioritizer/application/adapters"
	"prioritizer/prioritizer/application/job"
	applicationPrioritization "prioritizer/prioritizer/application/service/prioritization"
	"prioritizer/prioritizer/application/service/prioritization/handler"
	"prioritizer/prioritizer/application/service/prioritizationresult"
	"prioritizer/prioritizer/application/service/segmentdownloader"
	"prioritizer/prioritizer/domain"
	"prioritizer/prioritizer/domain/service/prioritization"
	"prioritizer/prioritizer/infrastructure/aws"
	"prioritizer/prioritizer/infrastructure/filesystem"
	"prioritizer/prioritizer/infrastructure/repository"
	"prioritizer/prioritizer/ui"
	"prioritizer/rabbitmq"
	"prioritizer/status"
	"syscall"
)

type Application struct {
	container *dig.Container
	config    *config.Config
}

func NewApplication(config *config.Config) *Application {
	app := &Application{
		config:    config,
		container: dig.New(),
	}
	app.initContainer()
	return app
}

func (app *Application) Run() {
	var err error

	switch cnf.AppType {
	case config.WebApp:
		var webApp fasthttp.RequestHandler
		webApp, err = app.getWepApp()
		if err == nil {
			err = fasthttp.ListenAndServe("0.0.0.0:8090", webApp)
		}
	case config.ScheduleApp:
		err = app.runScheduleApp()
	default:
		log.Fatal(fmt.Sprintf("unknown job type '%s'", string(cnf.AppType)))
	}
	if err != nil {
		log.Fatal(err.Error())
	}
}

func (app *Application) getWepApp() (handler fasthttp.RequestHandler, err error) {
	r := router.New()

	err = app.initHandlers(r)
	if err != nil {
		return
	}
	handler, err = app.wrapHandler(r.Handler)
	if err != nil {
		return
	}
	return handler, err
}

func (app *Application) runScheduleApp() (err error) {
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	/*
	err = app.container.Invoke(func(consumer *ui.PrioritizationStarterConsumer) error {
		return consumer.Consume()
	})
	if err != nil {
		log.Error(err.Error())
		return
	}
	*/

	log.Info("Schedule app is running")

	<-done
	log.Info("Schedule app stopped")

	return
}

func (app *Application) wrapHandler(next fasthttp.RequestHandler) (handler fasthttp.RequestHandler, err error) {

	handler = next
	err = app.container.Invoke(
		func(
			prometheusMiddleware *middleware.PrometheusMiddleware,
			swaggerMiddleware *middleware.SwaggerMiddleware,
		) {
			middlewareSlice := []middleware.Middleware{
				prometheusMiddleware,
				swaggerMiddleware,
			}
			for _, m := range middlewareSlice {
				handler = m.Process(handler)
			}
		})
	return
}

func (app *Application) initHandlers(r *router.Router) error {
	var err error

	err = app.container.Invoke(
		func(
			statusController *status.StatusController,
			metricsController *metrics.MetricsController,
			jobController *ui.JobController,
			prioritizationResultController *ui.PrioritizationResultController,
			launcher *applicationPrioritization.PrioritizationLauncher,
			prioritizationController *ui.PrioritizationController,
			releaseController *ui.ReleaseController,
		) {
			r.GET("/_/status", statusController.GetStatus)
			r.GET("/_/metrics", metricsController.GetMetrics)
			r.GET("/api/v1/job_get-by-id", jobController.GetById)
			r.POST("/api/v1/job_re-launch-by-id", jobController.ReLaunchById)
			r.GET("/api/v1/prioritization-result_get-by-job-id", prioritizationResultController.GetByJobId)
			r.POST("/api/v1/prioritization_start", prioritizationController.Start)
			r.POST("/api/v1/release_save-release-to-job", releaseController.SaveReleaseToJob)
		})

	return err
}

func (app *Application) initContainer() {
	err := multierr.Combine(
		app.container.Provide(func() *config.Config { return app.config }),

		app.container.Provide(func(cnf *config.Config) adapters.S3ClientInterface {
			s3Connection := aws.NewS3Connection(cnf)
			return aws.NewS3Client(s3Connection, cnf)
		}),

		app.container.Provide(func(cnf *config.Config) *db.DataBase {
			dataBase := new(db.DataBase)
			dataBase.InitConnections(cnf)
			return dataBase
		}),

		app.container.Provide(func(db *db.DataBase) *db.ConnectionPgSql { return db.ConnectionPgSql }),

		app.container.Provide(func() *metrics.Prometheus {
			hostName, _ := os.Hostname()
			return metrics.NewPrometheus("app", hostName)
		}),

		app.container.Provide(middleware.NewSwaggerMiddleware),
		app.container.Provide(middleware.NewPrometheusMiddleware),

		app.container.Provide(func(cnf *config.Config) *rabbitmq.RabbitMQ {
			rabbitMq := new(rabbitmq.RabbitMQ)
			rabbitMq.InitConnection(cnf)
			return rabbitMq
		}),

		app.container.Provide(func(rabbitmq *rabbitmq.RabbitMQ) *rabbitmq.ConnectionRabbitMQ {
			return rabbitmq.ConnectionRabbitMQ
		}),

		// domain
		app.container.Provide(func(
			jobsRepository domain.JobRepositoryInterface,
			userSendingsRepository domain.UserSendingRepositoryInterface,
			excludedUserSendingsRepository domain.ExcludedUserSendingRepositoryInterface,
			handlersChainFactory prioritization.HandlerFactoryInterface,
			uploadToStorageService *prioritizationresult.UploadToStorageService,
			cnf *config.Config,
		) prioritization.PrioritizationServiceInterface {
			return prioritization.NewPrioritizationService(
				jobsRepository,
				userSendingsRepository,
				excludedUserSendingsRepository,
				handlersChainFactory,
				uploadToStorageService,
				cnf.Prioritization.UsersBatchesGoroutinesCount,
				cnf.Prioritization.SingleUserGoroutinesCount,
				cnf.Prioritization.DeletedUsersToSaveBatchCount,
			)
		}),
		app.container.Provide(prioritization.NewCalculateAvailableLimits),

		// application
		app.container.Provide(job.NewJobService),

		app.container.Provide(handler.NewAdvReleaseHandler),
		app.container.Provide(handler.NewClientReleaseHandler),
		app.container.Provide(handler.NewApplyLimitsHandler),
		app.container.Provide(handler.NewCpkSortHandler),
		app.container.Provide(handler.NewOneSendingByTypeAndProductHandler),


		app.container.Provide(applicationPrioritization.NewHandlerChainFactory),
		app.container.Provide(applicationPrioritization.NewPrioritizationLauncher),

		app.container.Provide(prioritizationresult.NewGetPrioritizationResultsService),

		app.container.Provide(segmentdownloader.NewSegmentDownloader),

		app.container.Provide(func(
			userSendingRepository domain.UserSendingRepositoryInterface,
			prioritizationResultsRepositoryInterface domain.PrioritizationResultsRepositoryInterface,
			storageInterface adapters.S3ClientInterface,
			systemInterface adapters.FileSystemInterface,
		) *prioritizationresult.UploadToStorageService {
			return prioritizationresult.NewUploadToStorageService(
				userSendingRepository,
				prioritizationResultsRepositoryInterface,
				storageInterface,
				systemInterface,
			)
		}),

		// ui
		app.container.Provide(ui.NewJobController),
		app.container.Provide(ui.NewPrioritizationResultController),
		app.container.Provide(ui.NewPrioritizationController),
		app.container.Provide(ui.NewReleaseController),
		app.container.Provide(ui.NewPrioritizationStarterConsumer),

		// infrastructure
		app.container.Provide(repository.NewJobPostgresRepository),
		app.container.Provide(repository.NewUserSendingPostgresRepository),
		app.container.Provide(func(
			db *db.DataBase,
			cnf *config.Config,
		) domain.UserSendingRepositoryInterface {
			return repository.NewUserSendingPostgresRepository(
				db,
				cnf.Prioritization.MaxCountBatch,
				cnf.Prioritization.MaxCountUsersOnBatch,
			)
		}),
		app.container.Provide(repository.NewPrioritizationResultsPostgresRepository),
		app.container.Provide(repository.NewExcludedUserSendingPostgresRepository),
		app.container.Provide(filesystem.NewFileSystem),

		app.container.Provide(status.NewStatusController),
		app.container.Provide(metrics.NewMetricsController),
	)

	if err != nil {
		log.Fatal(err.Error())
	}
}
