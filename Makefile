up: compose-up migrate

compose-up:
	docker-compose up -d --build

down:
	docker-compose down --remove-orphans

run:
	go run golang-template

down-force:
	docker-compose down --remove-orphans -v

test: test-unit test-integration

test-unit:
	go test -v -cover -tags=unit ./...

test-integration:
	docker-compose run --rm test env CGO_ENABLED=0 go test -tags=integration -v -cover ./...

diff-test-check:
	mkdir -p report
	pip install 'diff_cover==3.0.1'
	go test -coverpkg=$(go list ./... | grep -E "domain|application" | tr '\n' ',' | sed 's/,$//') -v -tags=unit -coverprofile=coverage.out ./...
	go tool cover -func coverage.out
	go tool cover -html=coverage.out -o report/coverage.html
	gocover-cobertura < coverage.out > report/Cobertura.xml
	sed -i -- "s#filename=\"$(notdir $(shell pwd))/#filename=\"#g" report/Cobertura.xml
	diff-cover report/Cobertura.xml

install-migrate:
	if ! docker-compose exec app test -f "./migrate"; then docker-compose exec app wget -q http://nexus.action-media.ru/repository/github-raw/cicd/migrate/4.12.2/migrate && docker-compose exec app chmod +x migrate; fi

migrate-app:
	make install-migrate
	docker-compose exec app ./migrate -source file://migrations -database postgresql://postgres:postgres@db:5432/db?sslmode=disable up

migrate-test:
	make install-migrate
	docker-compose exec app ./migrate -source file://migrations -database postgresql://postgres:postgres@db:5432/db_test?sslmode=disable up

migrate: migrate-app migrate-test