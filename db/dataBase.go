package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"prioritizer/config"
	"time"
)

type DataBase struct {
	ConnectionPgSql             *ConnectionPgSql
	SegSegmentatorConnectionSql *ConnectionPgSql
	SegWebpannelConnectionDB    *ConnectionPgSql
}

type ConnectionPgSql struct {
	*gorm.DB
}

func (db *DataBase) InitConnections(config *config.Config) {
	db.initPgSqlConnection(config)

	db.initSegWebpannelSqlConnection(config)
	db.initSegSegmentatorSqlConnection(config)
}

func (db *DataBase) initPgSqlConnection(config *config.Config) {
	connStr := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config.PostgreSql.Host,
		config.PostgreSql.Port,
		config.PostgreSql.User,
		config.PostgreSql.Password,
		config.PostgreSql.DBName,
	)
	con, err := gorm.Open("postgres", connStr)
	if err != nil {
		log.Fatal("Error creating PgSQL Client:", err)
	}

	con.DB().SetMaxIdleConns(5)
	con.DB().SetMaxOpenConns(5)
	con.DB().SetConnMaxLifetime(time.Minute * 5)

	db.ConnectionPgSql = &ConnectionPgSql{con}
}

func (db *DataBase) initSegWebpannelSqlConnection(config *config.Config) {
	connStr := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config.SegWebpannelDb.Host,
		config.SegWebpannelDb.Port,
		config.SegWebpannelDb.User,
		config.SegWebpannelDb.Password,
		config.SegWebpannelDb.DBName,
	)
	con, err := gorm.Open("postgres", connStr)
	if err != nil {
		log.Fatal("Error creating SegWebpannel Client:", err)
	}

	con.DB().SetMaxIdleConns(5)
	con.DB().SetMaxOpenConns(5)
	con.DB().SetConnMaxLifetime(time.Minute * 5)

	db.SegWebpannelConnectionDB = &ConnectionPgSql{con}
}

func (db *DataBase) initSegSegmentatorSqlConnection(config *config.Config) {
	connStr := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config.SegSegmentatorDb.Host,
		config.SegSegmentatorDb.Port,
		config.SegSegmentatorDb.User,
		config.SegSegmentatorDb.Password,
		config.SegSegmentatorDb.DBName,
	)
	con, err := gorm.Open("postgres", connStr)
	if err != nil {
		log.Fatal("Error creating SegSegmentator Client:", err)
	}

	con.DB().SetMaxIdleConns(5)
	con.DB().SetMaxOpenConns(5)
	con.DB().SetConnMaxLifetime(time.Minute * 5)

	db.SegSegmentatorConnectionSql = &ConnectionPgSql{con}
}
