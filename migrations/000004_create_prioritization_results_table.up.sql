create table prioritization_results
(
    release_id  uuid not null,
    job_id      uuid not null,
    segment_url uuid not null,
    primary key (job_id, release_id)
);
