alter table jobs add column with_errors bool default false;
alter table jobs add column started_at timestamp;
alter table jobs add column ended_at timestamp;
alter table jobs add column users_count int;
alter table jobs add column sendings_count int;
alter table jobs add column deleted_sendings_count int;
