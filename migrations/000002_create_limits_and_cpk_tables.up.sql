create table user_sending_limits
(
    user_id      int not null,
    client_limit int not null,
    market_limit int not null,
    primary key (user_id)
);

create table user_product_cpk
(
    user_id    int not null,
    product_id int not null,
    value      int not null,
    primary key (user_id, product_id)
);
