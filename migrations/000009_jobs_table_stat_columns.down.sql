alter table jobs drop column with_errors;
alter table jobs drop column started_at;
alter table jobs drop column ended_at;
alter table jobs drop column users_count;
alter table jobs drop column sendings_count;
alter table jobs drop column deleted_sendings_count;
