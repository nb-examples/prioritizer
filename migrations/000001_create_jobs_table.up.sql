create table jobs
(
    id         uuid                     not null,
    status     varchar                  not null,
    created_at timestamp with time zone not null,
    updated_at timestamp with time zone not null,
    primary key (id)
);
