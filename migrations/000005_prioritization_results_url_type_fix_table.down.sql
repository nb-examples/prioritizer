alter table prioritization_results drop column segment_url;
alter table prioritization_results add column segment_url uuid;