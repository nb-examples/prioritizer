create table excluded_user_sendings
(
    id           uuid                     not null,
    job_id       uuid                     not null,
    release_id   uuid                     not null,
    user_id      int                      not null,
    user_email   varchar                  not null,
    segment_id   int                      not null,
    type         varchar                  not null,
    product_id   int                      not null,
    sending_time timestamp with time zone not null,
    primary key (id)
);
