// +build integration

package main

import (
	"github.com/gavv/httpexpect"
	"gitlab.action-media.ru/golang/log"
	"net/http"
	"testing"
)

var app = NewApplication(&cnf)

func getTester(t *testing.T) *httpexpect.Expect {
	handler, err := app.getWepApp()
	if err != nil {
		log.Fatal(err.Error())
	}
	return httpexpect.WithConfig(httpexpect.Config{
		Client: &http.Client{
			Transport: httpexpect.NewFastBinder(handler),
			Jar:       httpexpect.NewJar(),
		},
		Reporter: httpexpect.NewAssertReporter(t),
	})
}

func TestGetStatus(t *testing.T) {
	e := getTester(t)

	t.Run("success", func(t *testing.T) {
		response := e.GET("/_/status").Expect()

		response.Status(http.StatusOK)
	})
}
